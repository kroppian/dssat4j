# DSSAT Tools

DSSAT Tools is a Java wrapper for DSSAT. 

## Running experiments

##### Creating an Experiment
Necessary imports:
````java
import org.dsi.dssat4j.tools.DssatConfig;
import org.dsi.dssat4j.IO.Experiment;
import org.dsi.dssat4j.exceptions.FailedRunException;
import org.dsi.dssat4j.exceptions.FileParseException;
````

The DssatExperiment class allows Java to access and edit a DSSAT configuration (experiment) file.  The following code initializes and assigns an experiment object to a DSSAT experiment file:
```java
DssatExperiment exp = new DssatExperiment(config);
```
This action creates a new temporary experiment file that resembles the original experiment file. In this way, the original file is not accidentally modified.  

A ```IOException``` will be raised if the experiment file does not exist.  
A ```FileParseException``` will be raised if the experiment file cannot be parsed.  

The *parameter* to a ```DssatExperiment``` is a single ```DssatConfig``` object, which represents the DSSAT experiment file.  

```Java
 DssatConfig config = new DssatConfig(String installDir, String executable, CropTypes cropType, String instCode, String siteCode, int year, String expNumb);
```
For example, consider the full path: C:\DSSAT46\Maize\UFGA8203  
**installDir** is a String of the directory the configuration file is located (such as "C:\\DSSAT46\\"),  
**executable** is a String of the name of the executable file (such as "DSCSM046.exe"),  
**croptype** is of type CropTypes (for example: DssatConfig.CropTypes.MAIZE)  
**instCode** is a String that represents the institution location (for example, University of Florida is "UF")  
**siteCode** is a String of the site (for example, Georgia is "GA")  
**year** is an int of the year (for example, 1982 is 82)  
**expNumb** is a String of the experiment number (such as "03")  


#####Adding and Removing Treatments 
Necessary Imports:
```java
import org.dsi.dssat4j.beans.FertilizerApps;
import org.dsi.dssat4j.beans.IrrigationApps;
import org.dsi.dssat4j.exceptions.IllegalApplicationException;
```

New treatments may be added to the experiment file and existing treatments may be removed. To remove existing treatments, call ```clearTreatments()```.
Notice that this action (in addition to all further actions) does not actually modify the file itself. Instead, all modifications affect the temporary file that was created when the experiment object was initialized. 

To add new treatments, call ```addTreatment(IrrigationApps irrigation, FertilizerApps fertilizer)```. This action will add both the indicated irrigation and fertilizer applications to the experiment. ```addTreatment``` requires two parameters, one of type ```IrrigationApps```, and one of type ```FertilizerApps```. Each may be declared as follows: 
```java
IrrigationApps irrigation = new IrrigationApps();
FertilizerApps fertilizer = new FertilizerApps();
```
For example, an object of type ```IrrigationApps``` represents an irrigation application, which contains any number of irrigation treatments.  
Calling ```addIrrApp(int year, int month, int day, int amount)``` will add new applications to the ```IrrigationApps``` object. Any number of applications may be applied.  

This idea can be extended to ```FertilizerApps```. To add nitrogen, for example, ```addNitroApp(int year, int month, int day, int amount)``` can be called. Likewise, to add phosphorus, call ```addPhosApp(int year, int month, int day, int amount)```

A ```IllegalApplicationException``` will be raised if there is an illegal application execution when applying an application.  

Alternatively, the parameters of the ```addApp``` methods can also be expressed as follows:  
addApp(String date, int amount), which throws IllegalStringFormatException and IllegalApplicationException,  
or addApp(GregorianCalendar date, int amount), which throws IllegalStringFormatException and IllegalApplicationException.  
An IllegalApplicationException is thrown if there is more than one application per day.  


##### Running the Experiment
To run the experiment, create an object of type ```Outcome``` and assign it to ```exp.run()```, where ```exp``` is the experiment file. 
 ```java
Outcome outcome = exp.run();
```

In addition to the experiment file *(.MZX)*, both a *.MZA* and *.MDT* must exist for the experiment to successfully run.  

A ```IOException``` will be raised if there is an IO exception setting up the run environment.  
A ```FailedRunException``` will be raised if the run fails.  

The ```Outcome``` object can be used in a variety of ways. One useful method is the ```getYield()``` method, which will obtain the yield from the experiment. Another method, ```getNitrogenLeaching()```, will obtain the nitrogen leached.  

