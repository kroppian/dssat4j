package org.dsi.dssat4j.beans;

import java.util.List;

import org.dsi.dssat4j.tools.DssatTools;
import org.dsi.dssat4j.beans.ResourceApp.Substance;
import org.dsi.dssat4j.exceptions.IllegalStringFormatException;
import org.dsi.dssat4j.exceptions.IllegalApplicationException;

import java.util.GregorianCalendar;

public class FertilizerApps extends ResourceApps {

    DssatTools tools = new DssatTools();

    public FertilizerApps() {
        super(ApplicationType.FERTILIZER);
    }

    public FertilizerApps(List<ResourceApp> seed) {
        super(ApplicationType.FERTILIZER, seed);
    }

    @Override
    void addApp(int year, int month, int day, Substance subs, ResourceApp.Method method, int amount, int depth)
            throws IllegalApplicationException {

        boolean appWithSameDateExists = (this.getByDate(year, month, day).getBySubstance(subs).size() != 0);
        if (amount == 0) return;


        boolean moreThanOneAppOnDateWillExist = this.getByDate(year, month, day).getBySubstance(subs).size() > 0;
        if (moreThanOneAppOnDateWillExist)
            throw new IllegalApplicationException("More or less than one application for " + subs + " on " + year + "-" + month + "-" + day);

        this.data.add(new ResourceApp(year, month, day, subs, method, amount, depth));
    }

    @Override
    void addApp(String date, Substance subs,ResourceApp.Method method, int amount, int depth) throws IllegalStringFormatException, IllegalApplicationException {
        GregorianCalendar gregDate = tools.convert2Greg(date);
        int year = gregDate.get(GregorianCalendar.YEAR);
        int month = gregDate.get(GregorianCalendar.MONTH) + 1;
        int day = gregDate.get(GregorianCalendar.DAY_OF_MONTH);

        this.addApp(year, month, day, subs, method, amount, depth);

    }

    @Override
    void addApp(GregorianCalendar date, Substance subs, ResourceApp.Method method, int amount, int depth) throws IllegalStringFormatException, IllegalApplicationException {
        int year = date.get(GregorianCalendar.YEAR);
        int month = date.get(GregorianCalendar.MONTH) + 1;
        int day = date.get(GregorianCalendar.DAY_OF_MONTH);
        this.addApp(year, month, day, subs, method, amount, depth);
    }

    /*
     * Nitrogen Interface
     */
    public void addNitroApp(int year, int month, int day, ResourceApp.Method method, int amount, int depth) throws IllegalApplicationException {
        this.addApp(year, month, day, Substance.NITROGEN, method, amount, depth);
    }

    public void addNitroApp(String date, ResourceApp.Method method, int amount,  int depth) throws IllegalStringFormatException, IllegalApplicationException {
        this.addApp(date, Substance.NITROGEN, method, amount, depth);
    }

    public void addNirtroApp(GregorianCalendar date, ResourceApp.Method method, int amount, int depth) throws IllegalStringFormatException, IllegalApplicationException {
        this.addApp(date, Substance.NITROGEN, method, amount, depth);
    }

    /*
     * Phosphorus Interface
     */
    public void addPhosApp(int year, int month, int day, ResourceApp.Method method, int amount,  int depth) throws IllegalApplicationException {
        this.addApp(year, month, day, Substance.PHOSPHORUS, method, amount, depth);
    }

    public void addPhosApp(String date, ResourceApp.Method method, int amount, int depth) throws IllegalStringFormatException, IllegalApplicationException {
        this.addApp(date, Substance.PHOSPHORUS, method, amount, depth);
    }

    public void addPhosApp(GregorianCalendar date, ResourceApp.Method method, int amount, int depth) throws IllegalStringFormatException, IllegalApplicationException {
        this.addApp(date, Substance.PHOSPHORUS, method, amount, depth);
    }

    /*
     * Potassium Interface
     */
    public void addPotApp(int year, int month, int day, ResourceApp.Method method, int amount, int depth) throws IllegalApplicationException {
        this.addApp(year, month, day, Substance.POTASSIUM, method, amount, depth);
    }

    public void addPotApp(String date, ResourceApp.Method method, int amount, int depth) throws IllegalStringFormatException, IllegalApplicationException {
        this.addApp(date, Substance.POTASSIUM, method, amount, depth);
    }

    public void addPotApp(GregorianCalendar date, ResourceApp.Method method, int amount, int depth) throws IllegalStringFormatException, IllegalApplicationException {
        this.addApp(date, Substance.POTASSIUM, method, amount, depth);
    }

    // TODO calcium

}

