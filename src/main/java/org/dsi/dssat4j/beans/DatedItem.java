package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.exceptions.IllegalStringFormatException;
import org.dsi.dssat4j.tools.DssatTools;

import java.util.GregorianCalendar;


// TODO Should I throw an exception if the dates are out of range? 
public class DatedItem<T> implements Comparable<DatedItem> {

    private T item;

    DssatTools tools = new DssatTools();

    private GregorianCalendar date;

    public DatedItem(int year, int month, int day, T item) {
        this.date = new GregorianCalendar();
        // Correct Calendar's weird month index at 0 thing.
        this.date.set(year, --month, day);
        this.item = item;

    }

    public DatedItem(String date, T item) throws IllegalStringFormatException {
        this.date = new GregorianCalendar();
        // Correct Calendar's weird month index at 0 thing.
        this.setDate(date);
        this.item = item;
    }

    public DatedItem(GregorianCalendar date, T item) {
        this.date = date;
        // Correct Calendar's weird month index at 0 thing.
        this.setDate(date);
        this.item = item;
    }

    /*
     * Comparable
     */
    public int compareTo(DatedItem application) {
        int dateComp = this.date.compareTo(application.getDate());
        return dateComp;
    }

    /*
     * Getters
     */

    public GregorianCalendar getDate() {
        return this.date;
    }

    public int getYear() {
        return this.date.get(GregorianCalendar.YEAR);
    }

    public int getMonth() {
        // the +1 is to make up for Calendar's month index starting at 0
        return this.date.get(GregorianCalendar.MONTH) + 1;
    }

    public int getDayOfMonth() {
        return this.date.get(GregorianCalendar.DAY_OF_MONTH);
    }

    public T getItem(){
        return this.item;
    }

    /*
     * Setters
     */
    public void setDate(int year, int month, int day) {
        this.date = tools.convert2Greg(year, month, day);
    }

    public void setDate(GregorianCalendar date) {
        this.date = date;
    }

    public void setItem(T item){
        this.item = item;
    }

    /**
     * In the format yyddd (http://www.scp.byu.edu/docs/doychart.html)
     */
    public void setDate(String date) throws IllegalStringFormatException {
        this.date = tools.convert2Greg(date);
    }

    public void setYear(int year) {
        this.date.set(GregorianCalendar.YEAR, year);
    }

    public void setMonth(int month) {
        this.date.set(GregorianCalendar.MONTH, --month);
    }

    public void setDayOfMonth(int day) {
        this.date.set(GregorianCalendar.DAY_OF_MONTH, day);
    }


}



