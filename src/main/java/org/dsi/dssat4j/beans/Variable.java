package org.dsi.dssat4j.beans;

public interface Variable {

    public double getValue();

}
