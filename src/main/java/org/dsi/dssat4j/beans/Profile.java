package org.dsi.dssat4j.beans;

import java.util.*;

public class Profile {

    public class ProfileEntry {
        public String drive;
        public String directory;
        public String file;
        public String parameter;

        ProfileEntry(String drive, String directory, String file, String parameter){
            this.drive = drive;
            this.directory = directory;
            this.file = file;
            this.parameter = parameter;
        }

    }

    private List<String> entryKeys;
    private List<ProfileEntry> entries;

    public Profile(){
        this.entries = new ArrayList<ProfileEntry>();
        this.entryKeys = new ArrayList<String>();
    }


    public boolean addEntry(String key, String drive, String directory, String file, String parameter) {
        boolean notInSet = ! this.entryKeys.contains(key);
        if(notInSet){
            this.entryKeys.add(key);
            entries.add(new ProfileEntry(drive, directory, file, parameter));
        }
        return notInSet;
    }

    public ProfileEntry getEntry(String key){
        int i = this.entryKeys.indexOf(key);

        ProfileEntry result = null;
        if(i != -1)
            result = this.entries.get(i);

        return result;
    }

    public List<String> getEntrySet(){
        return this.entryKeys;
    }

}
