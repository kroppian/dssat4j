package org.dsi.dssat4j.beans;

public class Field {

    private Integer level;
    private String fieldId;
    private String weatherStation;
    private Integer slopeAspect;
    private Integer obstructionToSun;
    private String drainageType;
    private Integer drainDepth;
    private Integer drainSpacing;
    private String surfaceStones;
    private String soilTexture;
    private Integer soilDepth;
    private String soilID;
    private String fieldName;

    /*
     * Start -- Setters
     */
    public void setLevel(Integer level){
        this.level = level;
    }

    public void setFieldId(String fieldId){
        this.fieldId = fieldId;
    }

    public void setWeatherStation(String weatherStation){
        this.weatherStation = weatherStation;
    }

    public void setSlopeAspect(Integer slopeAspect){ this.slopeAspect = slopeAspect; }
    public void setObstructionToSun(Integer obstructionToSun){ this.obstructionToSun = obstructionToSun; }
    public void setDrainageType(String drainageType){ this.drainageType = drainageType; }
    public void setDrainDepth(Integer drainDepth){ this.drainDepth = drainDepth; }
    public void setDrainSpacing(Integer drainSpacing){ this.drainSpacing = drainSpacing; }
    public void setSurfaceStones(String surfaceStones){ this.surfaceStones = surfaceStones; }
    public void setSoilTexture(String soilTexture){ this.soilTexture = soilTexture; }
    public void setSoilDepth(Integer soilDepth){ this.soilDepth = soilDepth; }
    public void setSoilID(String soilID){ this.soilID = soilID; }
    public void setFieldName(String fieldName){ this.fieldName = fieldName; }



    /*
     * End -- Setters
     */

    /*
     * Start -- Getters
     */

    public int getLevel(){
        return this.level;
    }

    public String getFieldId(){
        return this.fieldId;
    }

    public String getWeatherStation(){
        return this.weatherStation;
    }

    public Integer getSlopeAspect(){ return this.slopeAspect; }
    public Integer getObstructionToSun(){ return this.obstructionToSun; }
    public String getDrainageType(){ return this.drainageType; }
    public Integer getDrainDepth(){ return this.drainDepth; }
    public Integer getDrainSpacing(){ return this.drainSpacing; }
    public String getSurfaceStones(){ return this.surfaceStones; }
    public String getSoilTexture(){ return this.soilTexture; }
    public Integer getSoilDepth(){ return this.soilDepth; }
    public String getSoilID(){ return this.soilID; }
    public String getFieldName(){ return this.fieldName; }

    /*
     * End -- Getters
     */

}
