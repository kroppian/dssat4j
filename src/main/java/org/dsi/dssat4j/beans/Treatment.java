package org.dsi.dssat4j.beans;

public class Treatment {

    private String description = "";
    private Integer treatmentNumber = 0;
    private Integer cultivar = 0;
    private Integer field = 0;
    private Integer soilAnalysis = 0;
    private Integer initialConditions = 0;
    private Integer plantManagement = 0;
    private Integer irrigationLevel = 0;
    private Integer fertilizerLevel = 0;
    private Integer residuleManagement = 0;
    private Integer chemicalApp = 0;
    private Integer tillage = 0;
    private Integer environmentalModel = 0;
    private Integer harvest = 0;
    private Integer simControl = 1;

    /*
     * Start -- Setters
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public void setTreatmentNumber(Integer treatmentNumber)  {
        this.treatmentNumber = treatmentNumber;
    }

    public void setCultivar(Integer cultivar){
        this.cultivar = cultivar;
    }

    public void setField(Integer field){
        this.field = field;
    }

    public void setSoilAnalysis(Integer soilAnalysis){
        this.soilAnalysis = soilAnalysis;
    }

    public void setInitialConditions(Integer initialConditions){
        this.initialConditions = initialConditions;
    }

    public void setPlantManagement(Integer plantManagement){
        this.plantManagement = plantManagement;
    }

    public void setIrrigationLevel(Integer irrigationLevel){
        this.irrigationLevel = irrigationLevel;
    }

    public void setFertilizerLevel(Integer fertilizerLevel) {
        this.fertilizerLevel = fertilizerLevel;
    }

    public void setResiduleManagement(Integer residuleManagement){
        this.residuleManagement = residuleManagement;
    }

    public void setChemicalApp(Integer chemicalApp){
        this.chemicalApp = chemicalApp;
    }

    public void setTillage(Integer tillage) {
        this.tillage = tillage;
    }

    public void setEnvironmentalModel(Integer environmentalModel){
        this.environmentalModel = environmentalModel;
    }

    public void setHarvest(Integer harvest) {
        this.harvest = harvest;
    }

    public void setSimControl(Integer simControl){
        this.simControl = simControl;
    }

    /*
     * End -- Setters
     */

    /*
     * Start -- Getters
     */
    public int getTreatmentNumber(){
        return this.treatmentNumber;
    }

    public String getDescription (){
        return this.description;
    }

    public int getCultivar(){
        return this.cultivar;
    }

    public int getField(){
        return this.field;
    }

    public int getSoilAnalysis(){
        return this.soilAnalysis;
    }

    public int getInitialConditions (){
        return this.initialConditions;
    }

    public int getPlantManagement (){
        return this.plantManagement;
    }

    public int getIrrigationLevel() {
        return this.irrigationLevel;
    }

    public int getFertilizerLevel() {
        return this.fertilizerLevel;
    }

    public int getResidueManagement(){
        return this.residuleManagement;
    }

    public int getChemicalApp(){
        return this.chemicalApp;
    }

    public int getTillage(){
        return this.tillage;
    }

    public int getEnvironmentalMOdel(){
        return this.environmentalModel;
    }

    public int getHarvest(){
        return this.harvest;
    }

    public int getSimControl(){
        return this.simControl;
    }

    /*
     *  End -- Getters
     */

    // TODO get the rest of the setters and getters


}
