package org.dsi.dssat4j.beans;

import java.util.GregorianCalendar;

import org.dsi.dssat4j.exceptions.IllegalStringFormatException;
import org.dsi.dssat4j.tools.DssatTools;

import java.lang.Comparable;


// TODO Should I throw an exception if the dates are out of range? 
public class ResourceApp implements Comparable<ResourceApp> {

    DssatTools tools = new DssatTools();

    public enum Substance {
        WATER, NITROGEN, PHOSPHORUS, POTASSIUM
    }

    // Checkout DETAILS.CDE in the DSSAT home directory for more details on these codes
    public enum Method {
        AP001, AP002, AP003, AP004, AP005, AP006, AP007, AP008, AP009,                  // Fertilizer methods
        AP011, AP012, AP013, AP014, AP015, AP016, AP017, AP018, AP019, AP020,
        IR001, IR002, IR003, IR004, IR005, IR006, IR007, IR008, IR009, IR010, IR011     // Irrigation methods
    }

    private Substance substance;

    private int amount;

    private int depth;

    private Method method;

    private GregorianCalendar applicationDate;

    public ResourceApp(int year, int month, int day, Substance subs, Method method, int amount, int depth) {
        this.substance = subs;
        this.amount = amount;
        this.applicationDate = new GregorianCalendar();
        // Correct Calendar's weird month index at 0 thing.
        this.applicationDate.set(year, --month, day);
        this.depth = depth;
        this.method = method;
    }

    public ResourceApp(String date, Substance subs, Method method, int amount, int depth) throws IllegalStringFormatException {
        this.substance = subs;
        this.amount = amount;
        this.applicationDate = new GregorianCalendar();
        // Correct Calendar's weird month index at 0 thing.
        this.setDate(date);
        this.depth = depth;
        this.method = method;
    }

    public ResourceApp(GregorianCalendar date, Substance subs, Method method, int amount, int depth) throws IllegalStringFormatException {
        this.substance = subs;
        this.amount = amount;
        this.applicationDate = date;
        // Correct Calendar's weird month index at 0 thing.
        this.setDate(date);
        this.depth = depth;
        this.method = method;
    }

    /*
     * Comparable
     */
    public int compareTo(ResourceApp application) {
        int dateComp = this.applicationDate.compareTo(application.getDate());
        if (dateComp == 0) {
            dateComp = this.getSubstance().compareTo(application.getSubstance());
        }
        return dateComp;
    }

    /*
     * Getters
     */
    public int getAmount() {
        return this.amount;
    }

    public GregorianCalendar getDate() {
        return this.applicationDate;
    }

    public Substance getSubstance() {
        return this.substance;
    }

    public Method getMethod() { return this.method; }

    public int getYear() {
        return this.applicationDate.get(GregorianCalendar.YEAR);
    }

    public int getMonth() {
        // the +1 is to make up for Calendar's month index starting at 0
        return this.applicationDate.get(GregorianCalendar.MONTH) + 1;
    }

    public int getDayOfMonth() {
        return this.applicationDate.get(GregorianCalendar.DAY_OF_MONTH);
    }

    public int getDepth(){ return this.depth; }

    /*
     * Setters
     */
    public void setDate(int year, int month, int day) {
        this.applicationDate = tools.convert2Greg(year, month, day);
    }

    public void setDate(GregorianCalendar date) {
        this.applicationDate = date;
    }

    /**
     * In the format yyddd (http://www.scp.byu.edu/docs/doychart.html)
     */
    public void setDate(String date) throws IllegalStringFormatException {
        this.applicationDate = tools.convert2Greg(date);
    }

    public void setYear(int year) {
        this.applicationDate.set(GregorianCalendar.YEAR, year);
    }

    public void setMonth(int month) {
        this.applicationDate.set(GregorianCalendar.MONTH, --month);
    }

    public void setDayOfMonth(int day) {
        this.applicationDate.set(GregorianCalendar.DAY_OF_MONTH, day);
    }

    public void setApplicationAmount(int amount) {
        this.amount = amount;
    }

    public void setDepth(int depth){ this.depth = depth; }

    public void setMethod(Method method) { this.method = method; }

}



