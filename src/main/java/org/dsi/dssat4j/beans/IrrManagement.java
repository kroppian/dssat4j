package org.dsi.dssat4j.beans;

public class IrrManagement {

    private int effectiveIrrigation;
    private int managementDepth;
    private int autoAppThresh;
    private int autoAppEndPercent;
    private String autoAppEndGrowth;
    private String  method;
    private int amountAutoIrrigation;
    private String irrigationName;

    public void setEffectiveIrrigation(int effectiveIrrigation){this.effectiveIrrigation = effectiveIrrigation;}
    public void setManagementDepth(int managementDepth){this.managementDepth = managementDepth;}
    public void setAutoAppThresh(int autoAppThresh){this.autoAppThresh = autoAppThresh;}
    public void setAutoAppEndPercent(int autoAppEndPercent){this.autoAppEndPercent = autoAppEndPercent;}
    public void setAutoAppEndGrowth(String autoAppEndGrowth){this.autoAppEndGrowth = autoAppEndGrowth;}
    public void setMethod(String method){this.method = method;}
    public void setAmountAutoIrrigation(int amountAutoIrrigation){this.amountAutoIrrigation = amountAutoIrrigation;}
    public void setIrrigationName(String irrigationName){this.irrigationName = irrigationName; }

    public int getEffectiveIrrigation(){ return this.effectiveIrrigation;}
    public int getManagementDepth(){ return this.managementDepth;}
    public int getAutoAppThresh(){ return this.autoAppThresh;}
    public int getAutoAppEndPercent(){ return this.autoAppEndPercent;}
    public String getAutoAppEndGrowth(){ return this.autoAppEndGrowth;}
    public String getMethod(){ return this.method;}
    public int getAmountAutoIrrigation(){ return this.amountAutoIrrigation;}
    public String getIrrigationName(){ return this.irrigationName; }


}
