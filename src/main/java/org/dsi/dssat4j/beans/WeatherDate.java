package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.exceptions.IllegalStringFormatException;
import org.dsi.dssat4j.exceptions.IllegalWeatherDataException;
import org.dsi.dssat4j.tools.DssatTools;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class WeatherDate {

    GregorianCalendar date;
    Double solarRadiation;
    Double maxTemperature;
    Double minTemperature;
    Double precipitaiton;

    public WeatherDate(int year, int month, int day, double srad, double tmax, double tmin, double precip)
            throws IllegalWeatherDataException{
        setDate(year,month, day, srad, tmax, tmin, precip);
    }

    public WeatherDate(GregorianCalendar date, double srad, double tmax, double tmin, double precip)
            throws IllegalWeatherDataException{
        setDate(date, srad, tmax, tmin, precip);
    }

    public WeatherDate(String yyddd, double srad, double tmax, double tmin, double precip)
            throws IllegalWeatherDataException{
        setDate(yyddd, srad, tmax, tmin, precip);
    }

    public void setDate(GregorianCalendar date, double srad, double tmax, double tmin, double precip)
            throws IllegalWeatherDataException {

        if (tmax <= tmin)
            throw new IllegalWeatherDataException("Invalid temperatures. tmin equal to or greater than tmax");

        this.date = date;
        this.solarRadiation = srad;
        this.maxTemperature = tmax;
        this.minTemperature = tmin;
        this.precipitaiton = precip;
    }

    public void setDate(int year, int month, int day, double srad, double tmax, double tmin, double precip)
            throws IllegalWeatherDataException {

        if (year < 1900 || year > 2200 || month < 1 || month > 12 || day < 1 || day > 31)
            throw new IllegalWeatherDataException("Invalid date: " + year + "-" + month + "-" + day);

        this.setDate(DssatTools.convert2Greg(year,month,day), srad, tmax, tmin, precip);

    }

    public void setDate(String yyddd, double srad, double tmax, double tmin, double precip)
            throws IllegalWeatherDataException{
        try {
            this.setDate(DssatTools.convert2Greg(yyddd), srad, tmax, tmin, precip);
        }catch(IllegalStringFormatException e){
            throw new IllegalWeatherDataException(e.getMessage() + "\n[" +  yyddd + "] is not in the proper yyddd format.");
        }

    }


    public GregorianCalendar getDate() {
        return date;
    }

    public Double getMaxTemperature() {
        return maxTemperature;
    }

    public Double getMinTemperature() {
        return minTemperature;
    }

    public Double getPrecipitaiton() {
        return precipitaiton;
    }

    public Double getSolarRadiation() {
        return solarRadiation;
    }


}
