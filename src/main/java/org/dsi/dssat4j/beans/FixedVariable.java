package org.dsi.dssat4j.beans;


class FixedVariable implements Variable {

    private double value;

    public FixedVariable(double value){
        this.value = value;
    }

    public FixedVariable(){
        this.value = 0;
    }


    public double getValue(){
        return value;
    }


}


