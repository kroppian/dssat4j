package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.exceptions.IllegalApplicationException;
import org.dsi.dssat4j.exceptions.IllegalStringFormatException;

import java.util.*;

public class DatedList<T> implements Iterable<DatedItem> {

    List<DatedItem> data;

    public DatedList(List<DatedItem> data){
        this.data = data;
    }

    public DatedList(){
        this.data = new ArrayList<DatedItem>();
    }

    @Override
    public Iterator<DatedItem> iterator() {

        return new Iterator<DatedItem>() {

            final Iterator<DatedItem> iter = data.iterator();

            @Override
            public boolean hasNext() {
                return iter.hasNext();
            }

            @Override
            public DatedItem next() {
                return iter.next();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("no changes allowed");
            }

        };

    }

    public void sort() {
        DateComparator comp = new DateComparator();
        this.data.sort(comp);
    }

    public void addItem(int year, int month, int day, T item){
        this.data.add(new DatedItem(year, month, day, item));
    }

    public void addItem(String date, T item) throws IllegalStringFormatException {
        this.data.add(new DatedItem(date, item));
    }

    public void addItem(GregorianCalendar date, T item)  {
        this.data.add(new DatedItem(date, item));
    }

    public void addItem(DatedItem item)  {
        this.data.add(item);
    }

    public List<DatedItem> toList() {
        return this.data;
    }

    public DatedList getByDate(int year) {
        List<DatedItem> result = new ArrayList<DatedItem>();
        for (DatedItem datum : this)
            if (datum.getYear() == year) result.add(datum);
        return new DatedList(result);
    }

    public DatedList getByDate(int year, int month) {
        List<DatedItem> result = new ArrayList<DatedItem>();
        for (DatedItem datum : this) {
            if (datum.getYear() == year && datum.getMonth() == month) result.add(datum);
        }
        return new DatedList(result);
    }

    public DatedList getByDate(int year, int month, int day) {
        List<DatedItem> result = new ArrayList<DatedItem>();
        for (DatedItem datum : this) {
            if (datum.getYear() == year && datum.getMonth() == month && datum.getDayOfMonth() == day) result.add(datum);
        }
        return new DatedList(result);
    }


    public DatedItem getFirst() {
        DatedItem result;
        if (this.data.size() == 0)
            result = null;
        else
            result = this.data.get(0);

        return result;
    }

    public DatedItem getItem(int i){
        return this.data.get(i);
    }

    public int size() {
        return this.data.size();
    }

    public void removeAll(DatedList data) {
        this.data.removeAll(data.toList());
    }

}

/*
 * Comparator
 */
class DateComparator implements Comparator<DatedItem> {

    @Override
    public int compare(DatedItem a, DatedItem b) {
        return a.compareTo(b);
    }


}


