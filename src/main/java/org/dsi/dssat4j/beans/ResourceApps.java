package org.dsi.dssat4j.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Iterator;
import java.util.GregorianCalendar;
import java.util.Comparator;

import org.dsi.dssat4j.exceptions.IllegalApplicationException;
import org.dsi.dssat4j.exceptions.IllegalStringFormatException;
import org.dsi.dssat4j.beans.ResourceApp.Substance;

public abstract class ResourceApps implements Iterable<ResourceApp> {

    public static enum ApplicationType {
        IRRIGATION, FERTILIZER
    }

    ApplicationType type;

    final List<ResourceApp> data = new ArrayList<ResourceApp>();

    public ResourceApps(ApplicationType type) {
        this.type = type;
    }

    public ResourceApps(ApplicationType type, List<ResourceApp> seed) {
        this.type = type;
        this.data.addAll(seed);
    }

    @Override
    public Iterator<ResourceApp> iterator() {

        return new Iterator<ResourceApp>() {

            final Iterator<ResourceApp> iter = data.iterator();

            @Override
            public boolean hasNext() {
                return iter.hasNext();
            }

            @Override
            public ResourceApp next() {
                return iter.next();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("no changes allowed");
            }

        };

    }

    private ResourceApps newInst() {
        if (this.type == ApplicationType.IRRIGATION)
            return new IrrigationApps();
        else
            return new IrrigationApps();
    }

    private ResourceApps newInst(List<ResourceApp> seed) {
        if (this.type == ApplicationType.IRRIGATION)
            return new IrrigationApps(seed);
        else
            return new IrrigationApps(seed);
    }

    public void sort() {
        AppComparator comp = new AppComparator();
        this.data.sort(comp);
    }

    abstract void addApp(int year, int month, int day, Substance subs, ResourceApp.Method method, int amount, int depth)
            throws IllegalApplicationException;

    abstract void addApp(String date, Substance subs, ResourceApp.Method method, int amount, int depth)
            throws IllegalStringFormatException, IllegalApplicationException;

    abstract void addApp(GregorianCalendar date, Substance subs, ResourceApp.Method method, int amount, int depth)
            throws IllegalStringFormatException, IllegalApplicationException;

    public List<ResourceApp> toList() {
        return this.data;
    }

    public ResourceApps getByDate(int year) {
        List<ResourceApp> result = new ArrayList<ResourceApp>();
        for (ResourceApp datum : this)
            if (datum.getYear() == year) result.add(datum);
        return newInst(result);
    }

    public ResourceApps getByDate(int year, int month) {
        List<ResourceApp> result = new ArrayList<ResourceApp>();
        for (ResourceApp datum : this) {
            if (datum.getYear() == year && datum.getMonth() == month) result.add(datum);
        }
        return newInst(result);
    }

    public ResourceApps getByDate(int year, int month, int day) {
        List<ResourceApp> result = new ArrayList<ResourceApp>();
        for (ResourceApp datum : this) {
            if (datum.getYear() == year && datum.getMonth() == month && datum.getDayOfMonth() == day) result.add(datum);
        }
        return newInst(result);
    }

    public ResourceApps getBySubstance(Substance subst) {
        List<ResourceApp> result = new ArrayList<ResourceApp>();
        for (ResourceApp datum : this) {
            if (datum.getSubstance() == subst) result.add(datum);
        }
        return newInst(result);
    }

    public ResourceApp getFirst() {
        ResourceApp result;
        if (this.data.size() == 0)
            result = null;
        else
            result = this.data.get(0);

        return result;
    }

    public int size() {
        return this.data.size();
    }

    public void removeAll(ResourceApps data) {
        this.data.removeAll(data.toList());
    }

}

/*
 * Comparator
 */
class AppComparator implements Comparator<ResourceApp> {

    @Override
    public int compare(ResourceApp a, ResourceApp b) {
        return a.compareTo(b);
    }


}


