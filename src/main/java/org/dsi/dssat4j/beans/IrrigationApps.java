package org.dsi.dssat4j.beans;

import java.util.List;

import org.dsi.dssat4j.tools.DssatTools;
import org.dsi.dssat4j.beans.ResourceApp.Substance;
import org.dsi.dssat4j.exceptions.IllegalStringFormatException;
import org.dsi.dssat4j.exceptions.IllegalApplicationException;

import java.util.GregorianCalendar;

public class IrrigationApps extends ResourceApps {

    DssatTools tools = new DssatTools();

    public IrrigationApps() {
        super(ApplicationType.IRRIGATION);
    }

    public IrrigationApps(List<ResourceApp> seed) {
        super(ApplicationType.IRRIGATION, seed);
    }

    @Override
    void addApp(int year, int month, int day, Substance subs, ResourceApp.Method method, int amount, int depth) throws IllegalApplicationException {
        if (this.getByDate(year, month, day).size() != 0)
            throw new IllegalApplicationException("More than one application for water on " + year + "-" + month + "-" + day);

        this.data.add(new ResourceApp(year, month, day, subs, method, amount, depth));
    }

    @Override
    void addApp(String date, Substance subs, ResourceApp.Method method, int amount, int depth) throws IllegalStringFormatException, IllegalApplicationException {
        GregorianCalendar gregDate = tools.convert2Greg(date);
        int year = gregDate.get(GregorianCalendar.YEAR);
        int month = gregDate.get(GregorianCalendar.MONTH) + 1;
        int day = gregDate.get(GregorianCalendar.DAY_OF_MONTH);

        if (this.getByDate(year, month, day).size() != 0)
            throw new IllegalApplicationException("More than one application for water on " + year + "-" + month + "-" + day);

        this.data.add(new ResourceApp(date, subs, method, amount, depth));
    }

    @Override
    void addApp(GregorianCalendar date, Substance subs, ResourceApp.Method method, int amount, int depth) throws IllegalStringFormatException, IllegalApplicationException {
        int year = date.get(GregorianCalendar.YEAR);
        int month = date.get(GregorianCalendar.MONTH) + 1;
        int day = date.get(GregorianCalendar.DAY_OF_MONTH);

        if (this.getByDate(year, month, day).size() != 0)
            throw new IllegalApplicationException("More than one application for water on " + year + "-" + month + "-" + day);

        this.data.add(new ResourceApp(date, subs, method, amount, depth));
    }

    public void addIrrApp(int year, int month, int day, ResourceApp.Method method, int amount) throws IllegalApplicationException {
        // Since irrigation depth isn't a variable in DSSAT, we'll just mark it down as -99
        this.addApp(year, month, day, Substance.WATER, method, amount, -99);
    }

    public void addIrrApp(String date, ResourceApp.Method method, int amount) throws IllegalStringFormatException, IllegalApplicationException {
        // Since irrigation depth isn't a variable in DSSAT, we'll just mark it down as -99
        this.addApp(date, Substance.WATER, method, amount, -99);
    }

    public void addIrrApp(GregorianCalendar date, ResourceApp.Method method, int amount) throws IllegalStringFormatException, IllegalApplicationException {
        // Since irrigation depth isn't a variable in DSSAT, we'll just mark it down as -99
        this.addApp(date, Substance.WATER, method, amount, -99);
    }


}

