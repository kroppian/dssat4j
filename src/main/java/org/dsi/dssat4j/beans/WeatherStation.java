package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.exceptions.IllegalWeatherDataException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

public class WeatherStation implements Iterable <WeatherDate>{

    private List<WeatherDate> dates = new ArrayList<WeatherDate>();

    private String institution = "    ";
    private Double latitude = 0.0;
    private Double longitude = 0.0;
    private Integer elevation = 0;
    private String weatherStationID = null; // First four digits of the file
    private String weatherStationFileID = null; // Last four digits of the file


    @Override
    public Iterator<WeatherDate> iterator() {

        return new Iterator<WeatherDate>() {

            final Iterator<WeatherDate> iter = dates.iterator();

            @Override
            public boolean hasNext() {
                return iter.hasNext();
            }

            @Override
            public WeatherDate next() {
                return iter.next();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("no changes allowed");
            }
        };
    }

    public WeatherStation(String weatherFile) throws IOException{
    }

    public WeatherStation(){
        dates = new ArrayList<WeatherDate>();
    }

    public void addDate(int year, int month, int day, double srad, double tmax, double tmin, double precip)
            throws IllegalWeatherDataException {

        dates.add(new WeatherDate(year, month, day, srad, tmax, tmin, precip));
    }

    public void addDate(GregorianCalendar date, double srad, double tmax, double tmin, double precip)
            throws IllegalWeatherDataException {

        dates.add(new WeatherDate(date, srad, tmax, tmin, precip));
    }

    public void addDate(String yyddd, double srad, double tmax, double tmin, double precip)
            throws IllegalWeatherDataException {
        dates.add(new WeatherDate(yyddd, srad, tmax, tmin, precip));
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public void setLatitude(Double latitude){
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setWeatherStationID(String weatherStationID){ this.weatherStationID = weatherStationID; }

    public void setWeatherStationFileID(String weatherStationFileID){ this.weatherStationFileID = weatherStationFileID; }

    public String getInstitution(){ return this.institution; }

    public Integer getElevation() { return this.elevation; }

    public Double getLatitude() { return this.latitude; }

    public Double getLongitude() { return this.longitude; }

    public String getWeatherStationID(){ return this.weatherStationID; }

    public String getWeatherStationFileID(){ return this.weatherStationFileID; }


}
