package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.IO.Outcome;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Economics {

    private final int VARIABLE_COUNT = 11;
    private final int PRICE_OF_GRAIN = 0;
    private final int PRICE_OF_BYPRODUCT = 1;
    private final int BASE_PRODUCTION_COST = 2;
    private final int N_FERTILIZER_COST = 3;
    private final int COST_PER_N_FERTILIZER_APPLICATION = 4;
    private final int IRRIGATION_COST = 5;
    private final int COST_PER_IRRIGATION_APPLICATION = 6;
    private final int SEED_COST = 7;
    private final int ORGANIC_AMENDMENTS =  8;
    private final int P_FERTILIZER_COST =  9;
    private final int COST_PER_P_FERTILIZER_APPLICATION =  10;

    HashMap<Integer, ArrayList<Variable>> treatments;

    public Economics(){
        treatments = new HashMap<Integer, ArrayList<Variable>>();
    }

    private ArrayList<Variable> initializeVariables(ArrayList<Variable> variables){
        for(int i = 0; i < VARIABLE_COUNT; i++){
            variables.add(new FixedVariable());
        }
        return variables;
    }

    private void setValue(Integer treatment,int index, double value){
        if(treatments.containsKey(treatment)){
            treatments.get(treatment).set(index,new FixedVariable(value));
        }else{
            treatments.put(treatment,initializeVariables(new ArrayList<Variable>(VARIABLE_COUNT)));
            treatments.get(treatment) .set(index, new FixedVariable(value));
        }
    }

    public void setGrainPrice(Integer treatment, double cost){
        setValue(treatment, PRICE_OF_GRAIN, cost);
    }

    public void setPriceOfByProduct(Integer treatment, double cost){
        setValue(treatment, PRICE_OF_BYPRODUCT, cost);
    }

    public void setBaseProductionCost(Integer treatment, double cost){
        setValue(treatment, BASE_PRODUCTION_COST, cost);
    }

    public void setNitrogenFertilizerCost(Integer treatment, double cost){
        setValue(treatment, N_FERTILIZER_COST, cost);
    }

    public void setCostPerNitrogenFertilizerApplication(Integer treatment, double cost){
        setValue(treatment, COST_PER_N_FERTILIZER_APPLICATION, cost);
    }

    public void setIrrigationCost(Integer treatment, double cost){
        setValue(treatment, IRRIGATION_COST, cost);
    }

    public void setCostPerIrrApplication(Integer treatment, double cost){
        setValue(treatment, COST_PER_IRRIGATION_APPLICATION, cost);
    }

    public void setSeedCost(Integer treatment, double cost){
        setValue(treatment, SEED_COST, cost);
    }

    public void setOrganicAmendments(Integer treatment, double cost){
        setValue(treatment, ORGANIC_AMENDMENTS, cost);
    }

    public void setPhosCost(Integer treatment, double cost){
        setValue(treatment, P_FERTILIZER_COST, cost);
    }

    public void setCostPerPhosApplied(Integer treatment, double cost){
        setValue(treatment, COST_PER_P_FERTILIZER_APPLICATION, cost);
    }

    public void setUniformDistrGrainPrice(int treatment, double lowerBound, double upperBound){
        // TODO
    }

    public void setTriangularDistrGrainPrice(int treatment, double lowerBound, double mode, double upperBound){
        // TODO
    }

    public void setNormalDistrGrainPrice(int treatment, double mean, double stdDev){
        // TODO
    }


    public double calculateGrossMargin(int treatment, Outcome outcome){

        List<Variable> variables = this.treatments.get(treatment);

        double result = 0;
        int i = 0;
        for(Variable var : variables){
            switch (i) {
                case PRICE_OF_GRAIN:
                    if(outcome.getHarvestedYield() != -99.0)
                        result += var.getValue() * outcome.getHarvestedYield();
                    break;
                case PRICE_OF_BYPRODUCT:
                    if(outcome.getByProductYield() != -99.0)
                        result += var.getValue() * outcome.getByProductYield();
                    break;
                case BASE_PRODUCTION_COST:
                    result -= var.getValue();
                    break;
                case N_FERTILIZER_COST:
                    if(outcome.getNitrogenApplied() != -99.0)
                        result -= var.getValue() * outcome.getNitrogenApplied();
                    break;
                case COST_PER_N_FERTILIZER_APPLICATION:
                    if(outcome.getNumberOfNitrogenApplications() != -99.0)
                        result -= var.getValue() * outcome.getNumberOfNitrogenApplications();
                    break;
                case IRRIGATION_COST:
                    if(outcome.getIrrigationApplied() != -99.0)
                        result -= var.getValue() * outcome.getIrrigationApplied();
                    break;
                case COST_PER_IRRIGATION_APPLICATION:
                    if(outcome.getNumberOfIrrigationApplictions() != -99.0)
                        result -= var.getValue() * outcome.getNumberOfIrrigationApplictions();
                    break;
                case SEED_COST:
                    if(outcome.getWeightOfPlantingMaterial() != -99.0)
                        result -= var.getValue() * outcome.getWeightOfPlantingMaterial();
                    break;
                case ORGANIC_AMENDMENTS:
                    if(outcome.getResidueApplied() != -99.0)
                        result -= var.getValue() * outcome.getResidueApplied();
                    break;
                case P_FERTILIZER_COST:
                    if(outcome.getPhosphorusApplied() != -99.0)
                        result -= var.getValue() * outcome.getPhosphorusApplied();
                    break;
                case COST_PER_P_FERTILIZER_APPLICATION:
                    if(outcome.getNumberOfPhosphorusApplications() != -99.0)
                        result -= var.getValue() * outcome.getNumberOfPhosphorusApplications();
                    break;
            }
            i++;
        }

        return result;
    }


}

