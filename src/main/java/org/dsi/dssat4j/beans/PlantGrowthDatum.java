package org.dsi.dssat4j.beans;

public class PlantGrowthDatum {

    private Double vegetativeWeight;
    private Double grainWeight;
    private Double topsWeight;
    private Double rootWeight;

    public PlantGrowthDatum(Double vegetativeWeight, Double grainWeight, Double topsWeight, Double rootWeight){
        this.vegetativeWeight = vegetativeWeight;
        this.grainWeight = grainWeight;
        this.topsWeight = topsWeight;
        this.rootWeight = rootWeight;
    }


    public void setVegetativeWeight(Double vegetativeWeight){
        this.vegetativeWeight = vegetativeWeight;
    }

    public void setGrainWeight(Double grainWeight){
        this.grainWeight = grainWeight;
    }

    public void setTopsWeight(Double topsWeight){
        this.topsWeight = topsWeight;
    }

    public void setRootWeight(Double rootWeight){
        this.rootWeight = rootWeight;
    }

    public Double getVegetativeWeight(){
        return this.vegetativeWeight;
    }

    public Double getGrainWeight(){
        return this.grainWeight;
    }

    public Double getTopsWeight(){
        return this.topsWeight;
    }

    public Double getRootWeight(){
        return this.rootWeight;
    }


}
