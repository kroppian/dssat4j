package org.dsi.dssat4j.exceptions;

import java.lang.Exception;

public class IllegalStringFormatException extends Exception {

    public IllegalStringFormatException() {
    }

    public IllegalStringFormatException(String message) {
        super(message);
    }

}

