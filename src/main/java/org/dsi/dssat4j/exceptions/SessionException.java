package org.dsi.dssat4j.exceptions;

public class SessionException extends Exception {

    public SessionException() {
    }

    public SessionException(String message) {
        super(message);
    }

}

