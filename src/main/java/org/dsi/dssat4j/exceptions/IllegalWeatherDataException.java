package org.dsi.dssat4j.exceptions;

public class IllegalWeatherDataException extends Exception {

    public IllegalWeatherDataException() {
    }

    public IllegalWeatherDataException(String message) {
        super(message);
    }

}

