package org.dsi.dssat4j.exceptions;

import java.lang.Exception;

public class FileParseException extends Exception {

    public FileParseException() {
    }

    public FileParseException(String message) {
        super(message);
    }


}



