package org.dsi.dssat4j.exceptions;

import java.lang.Exception;

public class IllegalApplicationException extends Exception {

    public IllegalApplicationException() {
    }

    public IllegalApplicationException(String message) {
        super(message);
    }


}



