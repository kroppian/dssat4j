package org.dsi.dssat4j.exceptions;

import java.lang.Exception;

public class FailedRunException extends Exception {

    public FailedRunException() {
    }

    public FailedRunException(String message) {
        super(message);
    }


}



