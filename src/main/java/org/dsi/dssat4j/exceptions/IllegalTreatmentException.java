package org.dsi.dssat4j.exceptions;

public class IllegalTreatmentException extends Exception {

    public IllegalTreatmentException() {
    }

    public IllegalTreatmentException(String message) {
        super(message);
    }


}



