package org.dsi.dssat4j.tools;
import org.apache.commons.io.FileUtils;
import org.dsi.dssat4j.exceptions.FailedRunException;
import org.dsi.dssat4j.exceptions.SessionException;

import java.io.File;
import java.io.IOException;


public class DssatSession {

    private final String S = File.separator;
    private final String SESH_ROOT_FOLDER = "dssatSessionRoot";
    private DssatConfig globalConf;
    private DssatConfig sessionConf;

    protected DssatSession(DssatConfig conf) throws SessionException {
        this.globalConf = conf.clone();
        this.setUpSession();
    }

    private RunIds ids;

    private void setUpSession() throws SessionException {

        final String S = File.separator;

        File tempExec = new File(globalConf.getTempDSSATRoot()  + S +
                                    SESH_ROOT_FOLDER + S +
                                    globalConf.getExecutable().getName());

        // Make sure that the executable has the right permissions
        try{
            FileUtils.copyFile(globalConf.getExecutable(), tempExec);
            if(System.getProperty("os.name").equals("Linux"))
                DssatTools.runCommand("chmod +x " + tempExec.getAbsolutePath(), "/");
        }catch (FailedRunException e){
            throw new SessionException("Unable to give the executable execute permissions.");
        }catch (IOException e){
            throw new SessionException("Error copying the executable to the temporary root location:\n" +
                    e.getMessage());

        }

        // This is the temporary root that will act as our session DSSAT root, so we can create create runners and copy
        // from our temp folder. This speeds stuff up if DSSAT session root is in ram disk (which is recommended)
        File tempDssatSessionRoot = new File(globalConf.getTempDSSATRoot() + S + SESH_ROOT_FOLDER);
        tearDownSession(tempDssatSessionRoot); // get rid of old sessions

        tempDssatSessionRoot.mkdir();

        // Updates conf with new root info

        this.sessionConf = DssatTools.setupDssatRoot(this.globalConf, tempDssatSessionRoot);
        this.sessionConf.setExecutable(tempExec.getAbsoluteFile());

        // Set up run ids (one ID per each run of DSSAT)
        this.ids = new RunIds();

    }

    protected void retireId(int id) {
        this.ids.retireId(id);
    }

    public void tearDownSession(File oldSession) throws SessionException {

        if(oldSession.exists()){
          try{
              FileUtils.deleteDirectory(oldSession);
          }catch(IOException e){
              throw new SessionException("Exception while trying to clear out old sesions");
          }
        }

    }

    public void closeRunner(DssatRunner toClose) throws SessionException {
        this.ids.retireId(toClose.getRunId());
        try{
            FileUtils.deleteDirectory(toClose.getRunPath());
        }catch(IOException e){
            throw new SessionException("Error deleting runner directory:\n" + e.getMessage());
        }
    }

    public DssatRunner getDssatRunner() throws SessionException {

        Integer runId = null;
        File runRoot = null;
        synchronized (this){
            runId =  this.ids.getId();


            runRoot =  new File(this.globalConf.getTempDSSATRoot().getAbsoluteFile() + S
                    + "RUN" + String.format("%04d", runId)  );

            if(runRoot.exists()){
              try{
                  FileUtils.deleteDirectory(runRoot);
              }catch(IOException e){
                  throw new SessionException("Error while trying to delete old RUN directory");
              }
            }

            runRoot.mkdir();

        }

        return new DssatRunner(runId, runRoot, this.sessionConf.getExecutable(),this);
    }

}
