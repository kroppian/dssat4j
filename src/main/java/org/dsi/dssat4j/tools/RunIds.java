package org.dsi.dssat4j.tools;

import java.util.ArrayList;
import java.util.List;

public class RunIds {

    List<Boolean> ids;

    public RunIds() {
        ids = new ArrayList<Boolean>();
    }

    public int getId() {

        int possibleId = 0;
        synchronized (this) {
            while (possibleId < ids.size() && ids.get(possibleId))
                possibleId++;

            if (possibleId >= ids.size())
                ids.add(true);
            else
                ids.set(possibleId, true);
        }
        return possibleId;

    }

    public void retireId(int id) {
        synchronized (this) {
            if (id < ids.size())
                ids.set(id, false);
        }
    }

}
