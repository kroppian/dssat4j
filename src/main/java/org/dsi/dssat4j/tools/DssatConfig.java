package org.dsi.dssat4j.tools;

import java.io.File;
import java.io.IOException;

// TODO this is more of an experiment config. Should rename class
public class DssatConfig {

    private final String S = File.separator;

    private File tempDSSATRoot;
    private File executable;
    private File profile;

    public DssatConfig(File runDirectory, File executable, File profile) throws IOException {
        if(!runDirectory.isDirectory())
            throw new IOException(String.format("Cannot find run directory given [%s]",runDirectory.toString()));
        this.tempDSSATRoot = runDirectory;

        if(!executable.exists() || executable.isDirectory())
            throw new IOException(String.format("Invalid executable path given [%s]",executable.toString()));
        this.executable = executable;

        if(!profile.exists() || profile.isDirectory())
            throw new IOException(String.format("Invalid profile path given [%s]",profile.toString()));
        this.profile = profile;
    }

    public File getExecutable(){ return this.executable; }
    public File getTempDSSATRoot(){ return tempDSSATRoot;}
    public File getProfilePath(){ return profile; }

    public void setExecutable(File executable){ this.executable = executable; }
    public void setTempDSSATRoot(File tempDSSATRoot){ this.tempDSSATRoot = tempDSSATRoot;}
    public void setProfilePath(File profile){  this.profile = profile; }

    public DssatConfig clone(){
        DssatConfig config = null;
        try{
            config = new DssatConfig(this.tempDSSATRoot,this.executable,this.profile);
        }catch(IOException e){
            // This shouldn't happen because the config shouldn't exist if the paths are bad
        }
        return config;
    }

}

