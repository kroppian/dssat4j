package org.dsi.dssat4j.tools;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.io.IOException;
import java.io.File;

import static java.nio.file.StandardCopyOption.*;

import java.util.concurrent.ThreadLocalRandom;

// TODO give the test file the same extention as the original file

public class TestTools {

    private final String S = File.separator;

    public File createTestFile(String originalFile) throws IOException {

        int randomNum = ThreadLocalRandom.current().nextInt(0, 9999);

        String testFile = "." + S + "src" + S + "test" + S + "resources" + S + "TEST" + String.format("%04d", randomNum) + ".MZX";

        Path source = Paths.get(originalFile);
        Path dest = Paths.get(testFile);

        Files.copy(source, dest, REPLACE_EXISTING);

        File newFile = new File(testFile);

        return newFile;

    }


}

