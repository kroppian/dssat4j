package org.dsi.dssat4j.tools;

import org.apache.commons.io.FileUtils;
import org.dsi.dssat4j.IO.Experiment;
import org.dsi.dssat4j.IO.Formatting;
import org.dsi.dssat4j.IO.Outcome;
import org.dsi.dssat4j.IO.Parsers;
import org.dsi.dssat4j.exceptions.FailedRunException;
import org.dsi.dssat4j.exceptions.FileParseException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;

import static org.dsi.dssat4j.tools.DssatTools.readBuffer;

public class DssatRunner {

    private final String S = File.separator;
    private int runId;
    private File runPath;
    private File exePath;
    private DssatSession session;
    private DssatTools tools;

    protected DssatRunner(int runId, File runPath, File exePath, DssatSession session){
        this.runId = runId;
        this.runPath = runPath;
        this.exePath = exePath;
        this.session = session;
        this.tools = new DssatTools();
    }

    public File getRunPath(){ return this.runPath; }

    public int getRunId(){ return this.runId; }

    public Outcome run(Experiment exp, int treatment) throws FailedRunException{

        if(exp.getTreatment(treatment) == null) {
            throw new FailedRunException("Experiment does not contain the given treatment");
        }

        Process dssatProc = null;

        // Setup exp file
        String expStr = Formatting.formatExpFile(exp);

        // Get the original experiment file name if available
        File origExp = exp.getOrigExpFile();
        if(origExp == null)
            origExp = new File("TEMP0000.MZX");            

        File newExpFile = new File(this.runPath + S +  origExp.getName());  // TODO change me
        Charset c = null;
        try{
            FileUtils.writeStringToFile(newExpFile, expStr, c);
        }catch(IOException e){
            throw new FailedRunException("Unable to write experiment to temp directory:\n" + e.getMessage());
        }

        // Setup the batch file
        File batchFile = new File(this.runPath + S +  "DSSBatch.v46");

        String command = null;
        if(System.getProperty("os.name").equals("Linux"))
            command = String.format("cd %s && chmod +x %s && %s b %s",
                    this.runPath, this.exePath.getAbsolutePath(), this.exePath.getAbsolutePath(), batchFile.getName());
        else
            command = String.format("cd %s & ",this.runPath) + this.exePath.getAbsolutePath() + " b " + batchFile.getName();

        /* Create a batch file in temporary directory  */
        String batchFileContents = "$BATCH(MAIZE)\n" +
                "!\n" +
                "! Directory    : " + this.runPath + "\n" +
                "! Command Line : " + this.exePath + " B DSSBatch.v46\n" +
                "!\n" +
                "@FILEX" +
                String.format("%88s", "") + "TRTNO     RP     SQ     OP     CO\n" +
                String.format("%-94s", newExpFile.getName()) +
                String.format("%5s", treatment) + "      1      0      0      0";

        try {
            FileWriter writer = new FileWriter(batchFile, false);
            writer.write(batchFileContents);
            writer.close();
        } catch (IOException e) {
            this.session.retireId(this.runId);
            throw new FailedRunException("IOException while writing to batch file:\n" + e.getMessage());
        }
        /* End -- Create a batch file in temporary directory  */

        // Run DSSAT :TADA:
        try {
            dssatProc = tools.runCommand(command, this.runPath.getPath());
        } catch (Exception e) {
            this.session.retireId(this.runId);
            throw new FailedRunException("Exception running DSSAT:\n" + e.getMessage());
        }

        if (dssatProc.exitValue() != 0) {
            // TODO clean this up
            String stdout = DssatTools.readBuffer(dssatProc.getInputStream());
            String stderr = DssatTools.readBuffer(dssatProc.getErrorStream());
            this.session.retireId(this.runId);
            throw new FailedRunException(String.format("Execution of DSSAT failed.\nStdout:\n%s\nStderr:\n%s",
                    stdout,
                    stderr));
        }

        Outcome outcome = null;
        try {
            outcome = new Outcome(this.runPath);
        } catch (FileParseException e) {
            this.session.retireId(this.runId);
            throw new FailedRunException("Run failed due to bad output file." +
                    "\r\n" + e.getMessage());
        } catch (IOException e){
            this.session.retireId(this.runId);
            throw new FailedRunException("IO Exception while reading the run output:\n" + e.getMessage());

        }


        this.session.retireId(this.runId);

        return outcome;
    }

    public void retireRunner() throws FailedRunException {
        try{
            FileUtils.deleteDirectory(this.runPath);
        }catch(IOException e){
            this.session.retireId(this.runId);
            throw new FailedRunException("IO Exception while trying to clean up run:\n" + e.getMessage());
        }
        this.session.retireId(this.runId);
    }


}
