package org.dsi.dssat4j.tools;

import org.dsi.dssat4j.exceptions.SessionException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DssatSessionFactory {

    List<DssatConfig> currentConfs;

    public DssatSessionFactory(){
        currentConfs = new ArrayList<>();
    }

    public DssatSession createSession(DssatConfig conf) throws SessionException {
        // Check whether or not there's a session running in the given folder
        for(DssatConfig currentConfs : currentConfs)
            if(currentConfs.getTempDSSATRoot().equals(conf.getTempDSSATRoot()))
                throw new SessionException("Cannot run two sessions at the same root.");

        // Inser config
        currentConfs.add(conf);

        DssatSession newSesh = null;
        try{
            newSesh = new DssatSession(conf) ;
        }catch(SessionException e){
            throw new SessionException("Exception while creating new session.\n" + e.getMessage());
        }

        return newSesh;

    }

}
