package org.dsi.dssat4j.tools;

import java.io.*;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.dsi.dssat4j.IO.Formatting;
import org.dsi.dssat4j.IO.Parsers;
import org.dsi.dssat4j.beans.Profile;
import org.dsi.dssat4j.exceptions.FailedRunException;
import org.dsi.dssat4j.exceptions.FileParseException;
import org.dsi.dssat4j.exceptions.IllegalStringFormatException;
import org.dsi.dssat4j.exceptions.SessionException;

import java.nio.charset.Charset;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * Created by kroppian on 6/19/2017.
 */
public class DssatTools {

    public static void patientFileCopier(File a, File b) throws IOException {
        int[] waitTimes = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
        boolean fileNotCopied = true;
        int i = 0;
        while (fileNotCopied) {
            try {
                FileUtils.copyFile(a, b);
                TimeUnit.SECONDS.sleep(waitTimes[i]);
                fileNotCopied = false;
            } catch (IOException e) {
                System.err.println("File transfer between " + a.getAbsolutePath() + " and " + b.getAbsolutePath() + " failed.");
                if (i > waitTimes.length) throw e;
                System.err.println("Trying again...");
                i++;
            } catch (InterruptedException e) {
            }
        }
    }

    public static String readBuffer(InputStream procBuffer) {
        StringBuilder sb  = new StringBuilder();
        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(procBuffer));

            String line = reader.readLine();

            while (line != null) {
                sb.append(line);
                line = reader.readLine();
            }

        } catch (IOException e) {

            System.err.println("Error reading the process buffer.");
            System.exit(1);

        }

        return sb.toString();

    }

    protected static DssatConfig setupDssatRoot(DssatConfig oldConf, File newRootPath) throws SessionException {

        File dssatProfile = oldConf.getProfilePath();
        /*
        Set up run directory
         */
        Charset chars = null;
        Profile originalProf = null;
        try{
            String profContents = FileUtils.readFileToString(dssatProfile, chars);
            originalProf = Parsers.parseProfile(profContents);
        }catch(IOException e){
            throw new SessionException("Error reading profile file during root setup:\n" + e.getMessage());
        }catch(FileParseException e){
            throw new SessionException("Error parsing the profile file during root setup:\n" + e.getMessage());
        }

        Profile newRootProf = new Profile();

        // Clear out out directories 



        for(String entryKey : originalProf.getEntrySet()){
            Profile.ProfileEntry entry =  originalProf.getEntry(entryKey);


            File oldPath;
            File oldDrive;
            if(entry.drive.equals(".")){
                String dirCleaned = entry.directory.replaceAll("^\\.[/\\\\]","");
                oldPath = new File(oldConf.getProfilePath().getAbsoluteFile().getParent() + File.separator + dirCleaned);
                if(System.getProperty("os.name").equals("Linux"))
                    oldDrive = new File("//");
                else
                    oldDrive = new File("C:\\");
            }else{
                oldPath = new File(entry.directory);
                oldDrive = new File(entry.drive);
            }

            if(!oldPath.exists() || oldPath.isFile()) {
                System.err.println(String.format("Warning: Couldn't find directory [%s]:", oldPath));
                continue; // Skip directories that don't exist. TODO send some warning message and/or log?
            }

            String newPathStr =  newRootPath.getAbsolutePath();

            // New path will be the same path as the old root, but just repositioned at the new root
            // E.G. Changing
            //      C:/original/dir
            // to the path C:/new/root/ will result in
            //      C:/new/root/original/dir
            //newPathStr = newPathStr.replace("C:\\","");
            File newPath = new File(newPathStr);

            newPath.mkdir();
            String newFile = null;

            if(entry.file != null){ // Are we looking at a file?


                if(entry.file.matches(".\\/?"))
                    continue;

                String fileCleaned = entry.file.replaceAll("^\\.[/\\\\]","");

                File fileToCopy = new File(oldPath + File.separator + fileCleaned);

                if(!fileToCopy.isFile()) {
                    System.err.println(String.format("Warning: File given [%s] is not a file", fileToCopy));
                    continue;
                }


                if(fileToCopy.exists()){
                    try{
                        // TODO add this as an optional logging thing
                        //System.out.println(String.format("copying file %s to new path %s",
                        //        fileToCopy.toString(),
                        //        newPath.toString()));
                        FileUtils.copyFileToDirectory(fileToCopy, newPath);
                    }catch(IOException e){
                        throw new SessionException(String.format("Exception while copy file %s to new path %s",
                                fileToCopy.toString(),
                                newPath.toString()));
                    }
                }// Otherwise, skip on to the next thing
                newFile = fileToCopy.getName().toString();

            }else{ // or a directory?
                // Copy all the files in the original directory (non-recursively)
                String[] filters = null;
                Iterator<File> iter = FileUtils.iterateFiles(oldPath,filters,false);

                while(iter.hasNext()){
                    File toCopy = new File(oldDrive + iter.next().toString());
                    try{
                        // TODO add this as an optional logging thing
                        //System.out.println(String.format("copying file %s to new path %s",
                        //        toCopy.toString(),
                        //        newPath.toString()));
                        FileUtils.copyFileToDirectory(toCopy, newPath);
                    }catch(IOException e){
                        //throw new SessionException("IO Exception while copying files over to new root:\n" + e.getMessage());
                        System.err.println(String.format("Warning: skipping file %s", toCopy.toString()));
                    }
                }

            }

            String parameter = originalProf.getEntry(entryKey).parameter;

            String newDrive;

            if(System.getProperty("os.name").equals("Linux")){
                newDrive = "//";
            }else{ // Assuming just linux or Windows
                newDrive = newPath.getAbsolutePath().substring(0,2);
            }

            newRootProf.addEntry(entryKey, newDrive, newPath.toString().replace("C:",""), newFile, parameter);

            /* Set up profile */

        }

        /* Create new profile*/
        DssatConfig newConf = oldConf.clone();
        File newProfile = new File(newRootPath.toString() + File.separator + oldConf.getProfilePath().getName());
        Charset c = null;
        try{
            FileUtils.writeStringToFile(newProfile,Formatting.formatProfile(newRootProf),  c);
        }catch(IOException e){
            throw new SessionException(String.format("Error writing new profile file [%s] in new root path [%s]",
                    newProfile.toString(),
                    newRootPath.toString()));
        }
        newConf.setProfilePath(newProfile);
        newConf.setTempDSSATRoot(newRootPath);

        return newConf;

    }

    public static Process runCommand(String cmd, String dir) throws FailedRunException {

        Process proc;

        try {
            File directory = new File(dir);
            if(System.getProperty("os.name").equals("Linux"))
                proc = Runtime.getRuntime().exec(new String[]{"sh", "-c", cmd}, new String[]{}, directory);
            else
                proc = Runtime.getRuntime().exec(new String[]{"cmd", "/c", cmd}, new String[]{}, directory);

            proc.waitFor();
        } catch (IOException e) {
            throw new FailedRunException("IO exception executing command.\n" + e.getMessage());
        } catch (InterruptedException e) {
            throw new FailedRunException("InterruptedException executing DSSAT command\n" + e.getMessage());
        }

        return proc;

    }

    /**
     * In the format yyddd (http://www.scp.byu.edu/docs/doychart.html)
     */
    public static GregorianCalendar convert2Greg(String yyddd) throws IllegalStringFormatException {

        GregorianCalendar result = new GregorianCalendar();

        if (yyddd.length() != 5) {
            throw new IllegalStringFormatException("Date format should be yyddd");
        }

        int year;
        int dayOfYear;
        try {
            year = Integer.parseInt(yyddd.substring(0, 2));
            dayOfYear = Integer.parseInt(yyddd.substring(2, 5));
        } catch (NumberFormatException e) {
            throw new IllegalStringFormatException("Bad date format. Date format should be yyddd");
        }

        // TODO figure out what the minimum date is
        if (year > 50)
            year += 1900;
        else
            year += 2000;

        result.set(GregorianCalendar.YEAR, year);

        int lastDayOfYear = 365;
        if (result.isLeapYear(year)) lastDayOfYear++;

        if (dayOfYear < 0 || dayOfYear > lastDayOfYear)
            throw new IllegalStringFormatException("Bad date format. Date format should be yyddd");

        result.set(GregorianCalendar.DAY_OF_YEAR, dayOfYear);

        return result;

    }

    public static GregorianCalendar convert2Greg(int year, int month, int day) {

        GregorianCalendar result = new GregorianCalendar();

        result.set(year, --month, day);

        return result;

    }

    public static String convert2YYDDD(GregorianCalendar date) {
        String year = Integer.toString(date.get(GregorianCalendar.YEAR)).substring(2, 4);
        String day = String.format("%03d", date.get(GregorianCalendar.DAY_OF_YEAR));
        return year + day;
    }



}

