package org.dsi.dssat4j.IO;

import org.dsi.dssat4j.beans.*;
import org.dsi.dssat4j.tools.DssatTools;

import javax.management.ListenerNotFoundException;
import java.io.FileWriter;
import java.util.GregorianCalendar;
import java.util.Map;

public class Formatting {

    private static DssatTools tools = new DssatTools();
    private static final String LINE_SEPARATOR = "\r\n";

    public static String formatEXPFileFertilizer(Map<Integer, FertilizerApps> fertilizerApplications) {

        GregorianCalendar defaultDate = new GregorianCalendar();
        defaultDate.set(1990, 0, 1);

        StringBuilder result = new StringBuilder();
        result.append("*FERTILIZERS (INORGANIC)" + LINE_SEPARATOR);
        result.append("@F FDATE  FMCD  FACD  FDEP  FAMN  FAMP  FAMK  FAMC  FAMO  FOCD FERNAME" + LINE_SEPARATOR);

        FertilizerApps fertApplication;
        int printed = 0;
        for (Integer fertAppsInd : fertilizerApplications.keySet()) {

            fertApplication = fertilizerApplications.get(fertAppsInd);

            fertApplication.sort();

            String amount;
            for (ResourceApp app : fertApplication) {

                String formatString = " %d %s FE001 %s %5d %5d %5d %5d     0     0   -99 -99" + LINE_SEPARATOR;

                if (app.getAmount() == 0) continue;
                if (app.getSubstance() == ResourceApp.Substance.NITROGEN) {
                    result.append(String.format(formatString,
                            fertAppsInd,
                            tools.convert2YYDDD(app.getDate()),
                            app.getMethod(),
                            app.getDepth(),
                            app.getAmount(),
                            0,0));
                } else if (app.getSubstance() == ResourceApp.Substance.PHOSPHORUS) {
                    result.append(String.format(formatString,
                            fertAppsInd,
                            tools.convert2YYDDD(app.getDate()),
                            app.getMethod(),
                            app.getDepth(),
                            0,
                            app.getAmount(),
                            0));
                } else if (app.getSubstance() == ResourceApp.Substance.POTASSIUM) {
                    result.append(String.format(formatString,
                            fertAppsInd,
                            tools.convert2YYDDD(app.getDate()),
                            app.getMethod(),
                            app.getDepth(),
                            0,0,
                            app.getAmount()));
                }
                printed++;
            }

            if (printed == 0)
                result.append(String.format(" %d %s FE001 AP001    10    0     0     0     0     0   -99 -99" +
                        LINE_SEPARATOR,
                        fertAppsInd,
                        tools.convert2YYDDD(defaultDate)));

            printed = 0;

        }

        return result.toString();

    }

    public static String formatEXPFileTreatments (Map<Integer, Treatment> treatments) {
        StringBuilder result = new StringBuilder("");

        result.append("*TREATMENTS                        -------------FACTOR LEVELS------------" + LINE_SEPARATOR);
        result.append("@N R O C TNAME.................... CU FL SA IC MP MI MF MR MC MT ME MH SM" + LINE_SEPARATOR);

        int i = 0;
        for(int treatmentNo : treatments.keySet()){
            Treatment currentTreatment = treatments.get(treatmentNo);
            String ls;
            if(i < treatments.keySet().size())
                ls = LINE_SEPARATOR;
            else
                ls = "";

            result.append(String.format("%2d", treatmentNo) + " 1 0 0 " +
                    String.format("%25s", currentTreatment.getDescription()) + "  " +
                    currentTreatment.getCultivar() + "  " +
                    currentTreatment.getField() + "  " +
                    currentTreatment.getSoilAnalysis() + "  " +
                    currentTreatment.getInitialConditions() + "  " +
                    currentTreatment.getPlantManagement() + "  " +
                    currentTreatment.getIrrigationLevel() + "  " +
                    currentTreatment.getFertilizerLevel() + "  " +
                    currentTreatment.getResidueManagement() + "  " +
                    currentTreatment.getChemicalApp() + "  " +
                    currentTreatment.getTillage() + "  " +
                    currentTreatment.getEnvironmentalMOdel() + "  " +
                    currentTreatment.getHarvest() + "  " +
                    currentTreatment.getSimControl() + ls
            );
            i++;
        }

        return result.toString();

    }

    public static String formatEXPFileIrrigation(Map<Integer, IrrigationApps> irrigationApplications, Map<Integer, IrrManagement> irrManagement) {

        String result = "";

        result += "*IRRIGATION AND WATER MANAGEMENT" + LINE_SEPARATOR;
        for(int treatment : irrigationApplications.keySet()){
            result += "@I  EFIR  IDEP  ITHR  IEPT  IOFF  IAME  IAMT IRNAME" + LINE_SEPARATOR;
            // TODO print the level
            IrrManagement man = irrManagement.get(treatment);
            result += String.format("%2d %5d %5d %5d %5d %5s %5s %5d %6s" + LINE_SEPARATOR,
                    treatment,
                    man.getEffectiveIrrigation(),
                    man.getManagementDepth(),
                    man.getAutoAppThresh(),
                    man.getAutoAppEndPercent(),
                    man.getAutoAppEndGrowth(),
                    man.getMethod(),
                    man.getAmountAutoIrrigation(),
                    man.getIrrigationName());
            result += "@I IDATE  IROP IRVAL" + LINE_SEPARATOR;

            IrrigationApps irrigationTreatment;

            irrigationTreatment = irrigationApplications.get(treatment);

            irrigationTreatment.sort();

            int i = 0;
            for (ResourceApp datum : irrigationTreatment) {
                result += " " + treatment + " " +
                        tools.convert2YYDDD(datum.getDate()) + " " +
                        datum.getMethod() + " " +
                        String.format("% 5d", datum.getAmount());

                if (i != irrigationApplications.size() - 1)
                    result += "\r\n";
                else
                    result += "\r\n";

                i++;
            }

            result = result.trim();
            result += LINE_SEPARATOR;

        }

        return result;

    }

    public static String formatEXPFileFields(Map<Integer, Field> fields){

        StringBuilder result = new StringBuilder();

        result.append("*FIELDS" + LINE_SEPARATOR);
        result.append("@L ID_FIELD WSTA....  FLSA  FLOB  FLDT  FLDD  FLDS  FLST SLTX  SLDP" +
                "  ID_SOIL    FLNAME" + LINE_SEPARATOR);

        for(int fieldId : fields.keySet()){
            Field currentField = fields.get(fieldId);

            result.append(String.format(
                    "%2d %-8s %-8s %5d %5d %5s %5d %5d %5s %-5s %4d  %-10s %-5s" + LINE_SEPARATOR,
                    currentField.getLevel(),
                    currentField.getFieldId(),
                    currentField.getWeatherStation(),
                    currentField.getSlopeAspect(),
                    currentField.getObstructionToSun(),
                    currentField.getDrainageType(),
                    currentField.getDrainDepth(),
                    currentField.getDrainSpacing(),
                    currentField.getSurfaceStones(),
                    currentField.getSoilTexture(),
                    currentField.getSoilDepth(),
                    currentField.getSoilID(),
                    currentField.getFieldName()
            ));

        }

        return result.toString();

    }

    public static String formatWTHFile(WeatherStation station){
        StringBuilder result = new StringBuilder();

        /* Station info */
        result.append("*WEATHER DATA :" + LINE_SEPARATOR);
        result.append(LINE_SEPARATOR);
        result.append("@ INSI      LAT     LONG  ELEV   TAV   AMP REFHT WNDHT" + LINE_SEPARATOR);

        result.append("  " + station.getInstitution() + " " +
            String.format("%8s", String.format("%3.3f", station.getLatitude())) + " " +
            String.format("%8s", String.format("%3.3f", station.getLongitude())) + " " +
            String.format("%5d", station.getElevation()) + "  " +
            // TODO replace this with actual data
            "20.9  13.0  2.00  3.00" + LINE_SEPARATOR
            );

        /* Weather data */
        result.append("@DATE  SRAD  TMAX  TMIN  RAIN" + LINE_SEPARATOR);

        for(WeatherDate date : station){
            result.append(tools.convert2YYDDD(date.getDate()) + " " +
                    String.format("%5s",String.format("%4.1f",date.getSolarRadiation())) + " " +
                    String.format("%5s",String.format("%4.1f",date.getMaxTemperature())) + " " +
                    String.format("%5s",String.format("%4.1f",date.getMinTemperature())) + " " +
                    String.format("%5s",String.format("%4.1f",date.getPrecipitaiton())) + " " +


                    LINE_SEPARATOR);
        }

        return result.toString();

    }


    public static String formatExpFile(Experiment exp){

        String totalMessage = exp.fileBeforeTreatment +
                Formatting.formatEXPFileTreatments(exp.getTreatments()) + LINE_SEPARATOR +
                exp.rawCultivarData +
                Formatting.formatEXPFileFields(exp.getFields()) +
                exp.rawWeatherStationData +
                exp.rawInitialConditionsData +
                exp.rawPlantingDetailsData +
                Formatting.formatEXPFileIrrigation(exp.getIrrigationTreatments(), exp.getIrrManagement()) + LINE_SEPARATOR +
                Formatting.formatEXPFileFertilizer(exp.getFertilizerTreatments()) +
                exp.fileAfterIrrigation;

        return totalMessage;

    }

    public static String formatProfile(Profile prof){
        StringBuilder sb = new StringBuilder();

        // compare line to line
        for(String key : prof.getEntrySet()){
            Profile.ProfileEntry entry = prof.getEntry(key);
            String drive = (entry.drive != null) ? entry.drive : "";
            String directory = (entry.directory != null) ? entry.directory : "";



            StringBuilder line = new StringBuilder();
            line.append(String.format("%s %s %s", key, drive, directory));

            if(entry.file != null) {
                line.append(String.format(" %s", entry.file));
                if(entry.parameter != null)
                    line.append(String.format(" %s", entry.parameter));
            }

            sb.append(line.toString().trim());
            sb.append("\r\n");
        }

        return sb.toString();

    }



}
