package org.dsi.dssat4j.IO;

import org.apache.commons.io.FileUtils;
import org.dsi.dssat4j.beans.DatedList;
import org.dsi.dssat4j.beans.PlantGrowthDatum;
import org.dsi.dssat4j.exceptions.FileParseException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

public class PlantGro {

    DatedList<PlantGrowthDatum> dates;

    public PlantGro(String fileName) throws IOException, FileParseException {

        File file = new File(fileName);
        String plantGroContents = FileUtils.readFileToString(file,Charset.defaultCharset());

        dates = Parsers.parsePlantGroFile(plantGroContents);

    }

    public void setDates(DatedList<PlantGrowthDatum> dates) {
        this.dates = dates;
    }

    public DatedList<PlantGrowthDatum> getDates(){
        return this.dates;
    }

}
