package org.dsi.dssat4j.IO;

import org.dsi.dssat4j.beans.*;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.HashMap;
import java.io.File;

/**
 * Created by kroppian on 6/22/2017.
 */

public class Experiment {

    /* Main data structures*/
    private Map<Integer, Treatment> treatments;
    private Map<Integer, IrrigationApps> irrApps;
    private Map<Integer, FertilizerApps> fertApps;
    private Map<Integer, Field> fields;
    private Map<Integer, IrrManagement> irrManagement;

    private GregorianCalendar defaultDate = new GregorianCalendar();

    private File origExpFile;

    // fields that don't have parsing code yet
    protected String fileBeforeTreatment;
    protected String rawCultivarData;
    protected String rawWeatherStationData;
    protected String rawInitialConditionsData;
    protected String rawPlantingDetailsData;
    protected String fileAfterIrrigation;

    public Experiment(){
        origExpFile = null;
        this.defaultDate.set(1990, 0, 1);
    }


    /*
     * Start -- Setter functions
     */

    public void setOrigExpFile(File exp){
      this.origExpFile = exp;
    }

    public void setField(int treatmentNo, Field field){
        if(this.fields.keySet().contains(treatmentNo))
            this.fields.remove(treatmentNo);

        this.fields.put(treatmentNo, field);
    }

    public void setFields(Map<Integer, Field> fields ){
        this.fields = fields;
    }

    public void setTreatment(Treatment treatment){
        this.treatments.put(treatment.getTreatmentNumber(), treatment);
    }

    public void setTreatments(Map<Integer, Treatment> treatments){
        this.treatments = treatments;
    }

    public void setIrrApps(Map<Integer, IrrigationApps> irrApps){
        this.irrApps = irrApps;
    }

    public void setFertApps(Map<Integer, FertilizerApps> fertApps){
        this.fertApps = fertApps;
    }

    public void setIrrigationLevel(int level, IrrigationApps apps){
        this.irrApps.put(level, apps);
    }

    public void setFertilizerLevel(int level, FertilizerApps apps){
        this.fertApps.put(level, apps);
    }

    public void setIrrManagement(Map<Integer, IrrManagement> man) { this.irrManagement = man; }
    public void setIrrManagement(int level, IrrManagement man) { this.irrManagement.put(level, man); }

    public void clearTreatments(){
        this.treatments = new HashMap<Integer, Treatment>();
    }

    public void clearIrrigationLevels(){
        this.irrApps = new HashMap<Integer, IrrigationApps>();
    }

    public void clearFertilizerLevels(){
        this.fertApps = new HashMap<Integer, FertilizerApps>();
    }
    /*
     * End -- Setter functions
     */

    /*
     * Start -- Getter functions
     */

    public File getOrigExpFile(){
      return this.origExpFile;
    }

    public Treatment getTreatment(int treatmentNo) {
        return this.treatments.get(treatmentNo);
    }

    public Map<Integer, Treatment> getTreatments(){
        return this.treatments;
    }

    public IrrigationApps getIrrigationLevel(int level){
        return this.irrApps.get(level);
    }

    public FertilizerApps getFertilizerLevel(int level){
        return this.fertApps.get(level);
    }

    public Map<Integer, IrrigationApps> getIrrigationTreatments() {
        return this.irrApps;
    }

    public Map<Integer, FertilizerApps> getFertilizerTreatments() {
        return this.fertApps;
    }

    public Map<Integer, IrrManagement> getIrrManagement(){ return this.irrManagement; }
    public IrrManagement getIrrManagement(int treatment){ return this.irrManagement.get(treatment); }

    public IrrigationApps getIrrigationTreatment(int treatmentNo) {
        return this.irrApps.get(treatmentNo);
    }

    public FertilizerApps getFertilizerTreatment(int treatmentNo) {
        return this.fertApps.get(treatmentNo);
    }

    public Map<Integer, Field> getFields(){
        return this.fields;
    }

    public Field getField(int treatmentNo){
        return this.fields.get(treatmentNo);
    }
    /*
     * End -- Getter functions
     */



}

