package org.dsi.dssat4j.IO;

import java.io.IOException;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.Charset;

import org.dsi.dssat4j.beans.DatedList;
import org.dsi.dssat4j.beans.PlantGrowthDatum;
import org.dsi.dssat4j.exceptions.FileParseException;


/**
 * Created by kroppian on 6/29/2017.
 */

public class Outcome {

    private final String OVERVIEW = "OVERVIEW.OUT";
    private final String SUMMARY = "Summary.OUT";
    private final String SOIL_INORGANIC_N_BALANCE = "SoilNiBal.OUT";
    private final String PLANT_GROWTH = "PlantGro.OUT";
    private final String S = File.separator;

    private static int HARVESTED_YIELD                     = 0;
    private static int BY_PRODUCT_YIELD                    = 1;
    private static int NITROGEN_APPLIED                    = 2;
    private static int NUMBER_OF_NITROGEN_APPLICATIONS     = 3;
    private static int IRRIGATION_APPLIED                  = 4;
    private static int NUMBER_OF_IRRIGATION_APPLICATIONS   = 5;
    private static int WEIGHT_OF_PLANTING_MATERIAL         = 6;
    private static int RESIDUE_APPLIED                     = 7;
    private static int PHOSPHORUS_APPLIED                  = 8;
    private static int NUMBER_OF_PHOSPHORUS_APPLICATIONS   = 9;

    private Integer yield;
    private Double leachedNO3;
    private DatedList<PlantGrowthDatum> plantGrowth;
    private Double[] summaryValues = new Double[Parsers.SUMMARY_CODE_POSITIONS_470.length];


    private String summaryFileContents;
    private String overviewFileContents;
    private String soilInorganicNBalanceFileContents;
    private String plantGrowthFileContents;

    // TODO pull this from experiment file
    private String crop = "Maize";

    public Outcome(File outputDir) throws IOException, FileParseException {

        File outcomeFile = new File(outputDir + S + OVERVIEW);
        File summaryFile = new File(outputDir + S + SUMMARY);
        File inorganicNBalanceFile = new File(outputDir + S + SOIL_INORGANIC_N_BALANCE);
        File plantGrowFile = new File(outputDir + S + PLANT_GROWTH);
        Charset charset = null;
        this.summaryFileContents = FileUtils.readFileToString(summaryFile,charset);
        this.overviewFileContents = FileUtils.readFileToString(outcomeFile,charset);
        this.soilInorganicNBalanceFileContents = FileUtils.readFileToString(inorganicNBalanceFile,charset);
        this.plantGrowthFileContents = FileUtils.readFileToString(plantGrowFile, charset);

        // Parse the outputfile
        this.summaryValues = Parsers.parseSummaryfile(this.summaryFileContents);
        this.yield = Parsers.parseOverviewFile(this.overviewFileContents);
        this.leachedNO3 = Parsers.parseSoilInorganicNBalanceFile(this.soilInorganicNBalanceFileContents);
        this.plantGrowth = Parsers.parsePlantGroFile(this.plantGrowthFileContents);

    }


    public int getYield() {
        return this.yield;
    }
    public double getNitroLeaching(){
        return this.leachedNO3;
    }

    public double getHarvestedYield(){
        return this.summaryValues[HARVESTED_YIELD];
    }

    public double getByProductYield(){
        return this.summaryValues[BY_PRODUCT_YIELD];
    }

    public double getNitrogenApplied(){
        return this.summaryValues[NITROGEN_APPLIED];
    }

    public double getNumberOfNitrogenApplications(){
        return this.summaryValues[NUMBER_OF_NITROGEN_APPLICATIONS];
    }

    public double getIrrigationApplied(){
        return this.summaryValues[IRRIGATION_APPLIED];
    }

    public double getNumberOfIrrigationApplictions(){
        return this.summaryValues[NUMBER_OF_IRRIGATION_APPLICATIONS];
    }

    public double getWeightOfPlantingMaterial(){
        return this.summaryValues[WEIGHT_OF_PLANTING_MATERIAL];
    }

    public double getResidueApplied(){
        return this.summaryValues[RESIDUE_APPLIED];
    }

    public double getPhosphorusApplied(){
        return this.summaryValues[PHOSPHORUS_APPLIED];
    }

    public double getNumberOfPhosphorusApplications(){
        return this.summaryValues[NUMBER_OF_PHOSPHORUS_APPLICATIONS];
    }

    public DatedList<PlantGrowthDatum> getPlantGrowth(){
        return this.plantGrowth;
    }

}


