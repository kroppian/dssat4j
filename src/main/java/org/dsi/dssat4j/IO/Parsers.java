package org.dsi.dssat4j.IO;

import org.dsi.dssat4j.beans.*;
import org.dsi.dssat4j.exceptions.FileParseException;
import org.dsi.dssat4j.exceptions.IllegalStringFormatException;
import org.dsi.dssat4j.exceptions.IllegalApplicationException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parsers {

    final static String LINE_SEPARATOR = "\r\n";
    final static String S = File.separator;
    /*
     * Experiment file
     */

    static protected Map<Integer, Treatment> parseEXPTreatments(String rawTreatmentData, Experiment exp) throws FileParseException {
        // TODO throw exception if irrigation or fertilizer data
        Map<Integer, Treatment> results = new HashMap<Integer, Treatment>();

        rawTreatmentData = rawTreatmentData.replaceAll("\\*TREATMENTS                        " +
                "-------------FACTOR LEVELS------------" + LINE_SEPARATOR, "");
        rawTreatmentData = rawTreatmentData.replaceAll("@N R O C TNAME.................... CU FL SA IC MP MI MF MR " +
                "MC MT ME MH SM" + LINE_SEPARATOR, "");

        for(String rawTreatment :  rawTreatmentData.split(LINE_SEPARATOR)){

            Treatment newTreatment = new Treatment();

            String description = rawTreatment.substring(9, 34);

            newTreatment.setDescription(description);

            // Get the fields bofore and after the description
            String fields [] = (rawTreatment.substring(0,9) + rawTreatment.substring(35)).trim().split(" +");

            if(fields.length != 17)
                throw new FileParseException("Expected 17 treatment fields. Found " + fields.length);

            int i = 0;
            for(String field : fields){

                String fieldName = "";
                try{
                    switch(i) {
                        case 0:
                            fieldName = "treatment number";
                            newTreatment.setTreatmentNumber(Integer.parseInt(field));
                            break;
                        case 4:
                            fieldName = "cultivar";
                            newTreatment.setCultivar(Integer.parseInt(field));
                            break;
                        case 5:
                            fieldName = "field";
                            newTreatment.setField(Integer.parseInt(field));
                            break;
                        case 6:
                            fieldName = "soil analysis";
                            newTreatment.setSoilAnalysis(Integer.parseInt(field));
                            break;
                        case 7:
                            fieldName = "initial conditions";
                            newTreatment.setInitialConditions(Integer.parseInt(field));
                            break;
                        case 8:
                            fieldName = "plant analysis";
                            newTreatment.setPlantManagement(Integer.parseInt(field));
                            break;
                        case 9:
                            fieldName = "irrigation management";
                            newTreatment.setIrrigationLevel(Integer.parseInt(field));
                            break;
                        case 10:
                            fieldName = "fertilizer management";
                            newTreatment.setFertilizerLevel(Integer.parseInt(field));
                            break;
                        case 11:
                            fieldName = "residue management";
                            newTreatment.setResiduleManagement(Integer.parseInt(field));
                            break;
                        case 12:
                            fieldName = "chemical app";
                            newTreatment.setChemicalApp(Integer.parseInt(field));
                            break;
                        case 13:
                            fieldName = "tillage";
                            newTreatment.setTillage(Integer.parseInt(field));
                            break;
                        case 14:
                            fieldName = "environmental model";
                            newTreatment.setEnvironmentalModel(Integer.parseInt(field));
                            break;
                        case 15:
                            fieldName = "harvest";
                            newTreatment.setHarvest(Integer.parseInt(field));
                        case 16:
                            fieldName = "simulation control";
                            newTreatment.setSimControl(Integer.parseInt(field));
                        default:
                            break;
                    }

                    int treatmentNo = Integer.parseInt(field);
                }catch(NumberFormatException e){
                    throw new FileParseException(e.getMessage() +
                            "\nError parsing the " + fieldName + " field. Expected an int, got " + fields[0]);
                }

                i++;
            }

            results.put(newTreatment.getTreatmentNumber(), newTreatment);

        }
        return results;
    }

    // year and expNo are used as fall backs in case we can't find the exp. file
    protected static Map<Integer, Field> parseEXPField(String rawFieldData) throws FileParseException {

        Map<Integer, Field> result = new HashMap<Integer, Field>();

        rawFieldData = rawFieldData.replaceAll("\\*FIELDS" + LINE_SEPARATOR, "");
        rawFieldData = rawFieldData.replaceAll("@L ID_FIELD WSTA....  FLSA  FLOB  FLDT  FLDD  FLDS  FLST SLTX  SLDP" +
                "  ID_SOIL    FLNAME" + LINE_SEPARATOR, "");

        for (String rawFieldEntry : rawFieldData.split(LINE_SEPARATOR)) {

            Field newField = new Field();

            String fieldCells[] = rawFieldData.trim().split(" +");
            int level;
            try {
                level = Integer.parseInt(fieldCells[0]);
            } catch (NumberFormatException e) {
                throw new FileParseException("Error parsing level data. Expected integer, got " + fieldCells[0]);
            }
            newField.setLevel(level);

            newField.setFieldId(fieldCells[1]);
            newField.setWeatherStation(fieldCells[2].trim());

            newField.setSlopeAspect(Integer.parseInt(fieldCells[3].trim()));
            newField.setObstructionToSun(Integer.parseInt(fieldCells[4].trim()));
            newField.setDrainageType(fieldCells[5].trim());
            newField.setDrainDepth(Integer.parseInt(fieldCells[6].trim()));
            newField.setDrainSpacing(Integer.parseInt(fieldCells[7].trim()));
            newField.setSurfaceStones(fieldCells[8].trim());
            newField.setSoilTexture(fieldCells[9].trim());
            newField.setSoilDepth(Integer.parseInt(fieldCells[10].trim()));
            newField.setSoilID(fieldCells[11].trim());
            newField.setFieldName(String.join(" ", Arrays.asList(Arrays.copyOfRange(fieldCells,12, fieldCells.length))));



            result.put(level, newField);


        }

        return result;

    }


    public static Experiment parseExpFile(File expFile) throws IOException, FileParseException {


        Experiment result = new Experiment();
    
        result.setOrigExpFile(expFile);

        result.rawCultivarData = "";
        result.rawWeatherStationData = "";
        result.rawInitialConditionsData = "";
        result.rawPlantingDetailsData = "";
        result.fileBeforeTreatment = "";
        result.fileAfterIrrigation = "";

        String irrApps = "";
        String fertApps = "";
        String rawTreatmentData = "";
        String rawFieldData = "";

        Path expFilePath = expFile.toPath();

        Charset charset = Charset.forName("UTF-8");
        BufferedReader expFileReader = Files.newBufferedReader(expFilePath, charset);

        boolean reachedTreatments = false;
        boolean readingTreatments = false;

        boolean reachedCultivars = false;
        boolean readingCultivars = false;

        boolean reachedFields = false;
        boolean readingFields = false;

        boolean reachedWeatherStations = false;
        boolean readingWeatherStations = false;

        boolean reachedInitialConditions = false;
        boolean readingInitialConditions = false;

        boolean reachedPlantingDetails = false;
        boolean readingPlantingDetails = false;

        boolean reachedIrrigationManagement = false;
        boolean readingIrrigationManagement = false;

        boolean reachedIrrigation = false;
        boolean readingIrrigation = false;

        boolean reachedFertilizer = false;
        boolean readingFertilizer = false;

        boolean reachedAllFields  = false;

        String line;
        // Loop through the experiment file, line by line, and extract the sections we want to parse
        while ((line = expFileReader.readLine()) != null) {

            /* Start - Check where we are in the file */
            // Check for Treatment headers
            if (!reachedTreatments && line.equals("*TREATMENTS                        " +
                    "-------------FACTOR LEVELS------------")) {
                reachedTreatments = true;
                readingTreatments = true;

                // Check for cultivars
            } else if (!reachedCultivars && line.equals("*CULTIVARS")) {
                readingTreatments = false;
                reachedCultivars = true;
                readingCultivars = true;

                // Check for fields
            } else if (!reachedFields && line.equals("*FIELDS")) {
                readingCultivars = false;
                reachedFields = true;
                readingFields = true;

                // Check for weather station
            } else if (!reachedWeatherStations && line.equals("@L ...........XCRD ...........YCRD .....ELEV " +
                    ".............AREA .SLEN .FLWR .SLAS FLHST FHDUR")) {
                readingFields = false;
                reachedWeatherStations = true;
                readingWeatherStations = true;

                // Check for initial conditions
            } else if (!reachedInitialConditions && line.equals("*INITIAL CONDITIONS")) {
                readingWeatherStations = false;
                reachedInitialConditions = true;
                readingInitialConditions = true;

                // Check for planting details
            } else if (!reachedPlantingDetails && line.equals("*PLANTING DETAILS")) {
                readingInitialConditions = false;
                reachedPlantingDetails = true;
                readingPlantingDetails = true;

                // Check for irrigation management header
            } else if (!reachedIrrigation && line.equals("*IRRIGATION AND WATER MANAGEMENT")) {
                readingPlantingDetails = false;
                reachedIrrigation = true;
                readingIrrigation = true;

                // Check for fertilizer header
            } else if (!reachedFertilizer && line.equals("*FERTILIZERS (INORGANIC)")) {
                readingIrrigation = false;
                reachedFertilizer = true;
                readingFertilizer = true;

                reachedAllFields = true;
            } else if (reachedAllFields && line.matches("^\\W*$")) {
                readingFertilizer = false;
            }
            /* End - Check where we are in the file */

            /* Start - Record this particular part of the file */
            if (readingTreatments)
                rawTreatmentData                    += line + "\r\n";
            else if (readingCultivars)
                result.rawCultivarData              += line + "\r\n";
            else if (readingFields)
                rawFieldData                        += line + "\r\n";
            else if (readingWeatherStations)
                result.rawWeatherStationData        += line + "\r\n";
            else if (readingInitialConditions)
                result.rawInitialConditionsData     += line +  "\r\n";
            else if (readingPlantingDetails)
                result.rawPlantingDetailsData       += line + "\r\n";
            else if (readingIrrigation)
                irrApps                             += line + "\r\n";
            else if (readingFertilizer)
                fertApps                            += line + "\r\n";
            else if (reachedAllFields)
                result.fileAfterIrrigation          += line + "\r\n";
            else
                result.fileBeforeTreatment          += line + "\r\n";

            /* End - Record this particular part of the file */

        }
        expFileReader.close();

        IrrigationResult irrResults =  Parsers.parseEXPIrrigation(irrApps);

        result.setIrrApps(irrResults.irrigation);
        result.setIrrManagement(irrResults.irrigationMan);

        result.setFertApps(Parsers.parseEXPFertilizer(fertApps));

        result.setTreatments(Parsers.parseEXPTreatments(rawTreatmentData, result));

        result.setFields(Parsers.parseEXPField(rawFieldData));

        return result;

    }


    public static Map<Integer, IrrManagement> parseEXPIrrigationManagement(String rawIrrManData)
            throws FileParseException {
        Map<Integer, IrrManagement> result = new HashMap<Integer, IrrManagement>();
        String[] lines = rawIrrManData.split("\r\n");
        for(int i = 0; i < lines.length; i++){
            String[] fields = lines[i].trim().split("\\s+");
            IrrManagement man = new IrrManagement();
            man.setEffectiveIrrigation(Integer.parseInt(fields[1]));
            man.setManagementDepth(Integer.parseInt(fields[2]));
            man.setAutoAppThresh(Integer.parseInt(fields[3]));
            man.setAutoAppEndPercent(Integer.parseInt(fields[4]));
            man.setAutoAppEndGrowth(fields[5]);
            man.setMethod(fields[6]);
            man.setAmountAutoIrrigation(Integer.parseInt(fields[7]));
            man.setIrrigationName(String.join(" ", Arrays.asList(Arrays.copyOfRange(fields,8, fields.length))));
            result.put(Integer.parseInt(fields[0]), man);
        }
        return result;
    }

    static class IrrigationResult {
        Map<Integer, IrrManagement> irrigationMan;
        Map<Integer, IrrigationApps> irrigation;
    }

    protected static IrrigationResult parseEXPIrrigation(String rawIrrData) throws FileParseException {

        IrrigationResult result = new IrrigationResult();

        result.irrigation = new HashMap<Integer, IrrigationApps>();
        result.irrigationMan = new HashMap<Integer, IrrManagement>();

        rawIrrData = rawIrrData.replaceAll("^\\*IRRIGATION AND WATER MANAGEMENT\r\n", "");

        String levelToken = "@I  EFIR  IDEP  ITHR  IEPT  IOFF  IAME  IAMT IRNAME";
        String[] levelHeaders;

        StringBuilder rawIrrManagement = new StringBuilder();

        int level = 1;
        for (String section : rawIrrData.split(levelToken)) {

            List<String> lines = new ArrayList<String>(Arrays.asList(section.split("\r\n")));

            // Check to make sure we have a valid number of rows
            if (lines.size() < 4) continue;

            // Remove the first line if it's blank
            if (lines.get(0).matches("^\\W*$")) lines.remove(0);

            // Next line should be a header. Split up the words
            levelHeaders = lines.get(0).replaceAll("^\\s*", "").split("\\s+");


            // Add this to our management lines and then pitch it
            rawIrrManagement.append(lines.get(0) + LINE_SEPARATOR);
            lines.remove(0);

            // Next should be the data header, which we don't need
            if (!lines.get(0).equals("@I IDATE  IROP IRVAL")) {
                throw new FileParseException("Unexpected line. Was expecting [@I IDATE  IROP IRVAL], got [" + lines.get(0) + "]");
            }
            lines.remove(0);

            result.irrigation.put(level, new IrrigationApps());

            for (String line : lines) {
                // skip blanks
                if (line.matches("^\\W*$")) continue;

                List<String> fields = new ArrayList<String>(
                        Arrays.asList(line.replaceAll("^ *", "").split("\\W+"))
                );

                if (fields.size() != 4)
                    throw new FileParseException("Too many fields.\n" + line);

                // 1. Level field
                if (!fields.get(0).equals(Integer.toString(level)))
                    throw new FileParseException("Level number didn't match. [" + fields.get(0) + "] vs ["
                            + Integer.toString(level) + "]\n" + line );

                fields.remove(0);

                // 2. Date field
                String date = fields.remove(0);

                // 3. Operation field
                String op = fields.remove(0);

                ResourceApp.Method method = ResourceApp.Method.valueOf(op);

                // 4. Amount field
                String amount = fields.remove(0);

                // Add to our data object
                try {
                    result.irrigation.get(level).addIrrApp(date, method, Integer.parseInt(amount));
                } catch (IllegalStringFormatException e) {
                    throw new FileParseException("Date string not valid.\n" + rawIrrData);
                } catch (NumberFormatException e) {
                    throw new FileParseException("Irrigation amount not valid.\n" + rawIrrData);
                } catch (IllegalApplicationException e) {
                    throw new FileParseException(e.getMessage() + "\nTwo irrigation applications occured on the same day.\n" + rawIrrData);
                }

            }

            level++;

            // TODO right now, we're only going to do one level. Adding a new level
            // will involve redesigning the datum object

        }

        result.irrigationMan = parseEXPIrrigationManagement(rawIrrManagement.toString());

        return result;

    }

    protected static Map<Integer, FertilizerApps> parseEXPFertilizer(String rawFertilizerData) throws FileParseException {

        Map<Integer, FertilizerApps> result = new HashMap<Integer, FertilizerApps>();

        if (rawFertilizerData.length() == 0) return result;

        // First line should be the title
        rawFertilizerData = rawFertilizerData.replaceAll("^\\**FERTILIZERS (INORGANIC)\r\n", "");

        String[] headers;

        List<String> lines = new ArrayList<String>(Arrays.asList(rawFertilizerData.split("\r\n")));

        String columnHeaders = "@F FDATE  FMCD  FACD  FDEP  FAMN  FAMP  FAMK  FAMC  FAMO  FOCD FERNAME";

        // Next line should be the column headers
        if (lines.get(0).matches(columnHeaders))
            throw new FileParseException("The fertilizer column header should be the following:\n" +
                    columnHeaders + "\nReceived:\n" + lines.get(0).matches(columnHeaders));

        lines.remove(0);

        String[] columns;
        columns = lines.get(0).replaceAll("^\\s*", "").split("\\s+");

        if (columns.length != 12)
            throw new FileParseException("Fertilizer data should have 12 columns. Found " + columns.length);

        lines.remove(0);

        // TODO this should start at zero
        int level = 1;
        result.put(level, new FertilizerApps());
        for (int i = 0; i < lines.size(); i++) {

            columns = lines.get(i).replaceAll("^\\s*", "").split("\\s+");

            int levelFound;
            try {
                levelFound = Integer.parseInt(columns[0]);
            } catch (NumberFormatException e) {
                throw new FileParseException("Invalid level number in column one. Expected " + level + ", found [" + columns[0] + "]");
            }

            if (levelFound == (level + 1)) {
                level++;
                result.put(level, new FertilizerApps());
            } else if (levelFound != level) {
                throw new FileParseException("Out of order level number in column one. Expected " + level + ", found " + levelFound);
            }

            String date = columns[1];
            String materialStr = columns[2];
            String methodStr = columns[3];
            String depthStr = columns[4];
            String nitrogenStr = columns[5];
            String phosphorusStr = columns[6];
            String potassiumStr = columns[7];
            String calciumStr = columns[8];
            String otherElementsStr = columns[9];
            String otherElementsCodeStr = columns[10];

            // Confirm that the the application numbers given are integers
            int depth;
            int nitrogen;
            int phosphorus;
            int potassium;
            int calcium;
            ResourceApp.Method method;
            try {
                method = ResourceApp.Method.valueOf(methodStr);
                depth = Integer.parseInt(depthStr);
                nitrogen = Integer.parseInt(nitrogenStr);
                phosphorus = Integer.parseInt(phosphorusStr);
                potassium = Integer.parseInt(potassiumStr);
                calcium = Integer.parseInt(calciumStr);
            } catch (NumberFormatException e) {
                throw new FileParseException("Invalid fertilizer amounts given: [" +
                        nitrogenStr + "," + phosphorusStr + "," + potassiumStr + "," + calciumStr + "]");
            }

            // Plug the fertilizer data into the application object
            try {
                result.get(level).addNitroApp(date, method, nitrogen, depth);
                result.get(level).addPhosApp(date, method, phosphorus, depth);
                result.get(level).addPotApp(date, method, potassium, depth);
            } catch (IllegalStringFormatException e) {
                throw new FileParseException("The given date [" + date + "] is invalid.\n" + e.getMessage());
            } catch (IllegalApplicationException e) {
                throw new FileParseException("The given line [" + lines.get(0) + "] is an invalid application.\n" + e.getMessage());
            }


        }

        return result;
    }

    /*
     * Outcome file
     */

    private static String crop = "Maize";
    protected static int parseOverviewFile(String overviewFile) throws FileParseException {

        int result;

        // Split by section headers
        String[] sections = overviewFile.split("\\r?\\n\\*[^\\*].*");

        if (sections.length < 1)
            throw new FileParseException("Overview files does not have enough sections: [" + sections.length + "]");

        String resourceProductivity = sections[sections.length - 1];

        // Example of what we're tryign to pull out
        // Maize YIELD :     8668 kg/ha    [Dry weight]
        Pattern pattern = Pattern.compile(crop + "\\s+YIELD\\s:\\s+\\d+\\s+kg/ha\\s+\\[Dry weight\\]");
        Matcher matcher = pattern.matcher(resourceProductivity);

        String line = "";
        if (matcher.find()) {
            line = matcher.group(0);
        } else {
            throw new FileParseException("Last section does not contain Yield:\r\n[" + resourceProductivity + "]");
        }

        line = line.replaceAll("^[^\\d]+", "");
        line = line.replaceAll("[^\\d]*$", "");


        try {
            result = Integer.parseInt(line);
        } catch (NumberFormatException e) {
            throw new FileParseException("Cannot find yield number in last section.\r\n[" + resourceProductivity + "]");
        }
        return result;
    }


    /*
     * Summary file
     */
    //                                                "HWAH", "BWAH",  "NICM", "NI#M", "IRCM", "IR#M", "DWAP", "RECM", "PICM", "PI#M"
    public static int[] SUMMARY_CODE_POSITIONS_470 =  {172,    181,     290,    284,    236,    230,    150,    380,    338,    332 };
    //                                                "HWAH", "BWAH",  "NICM", "NI#M", "IRCM", "IR#M", "DWAP", "RECM", "PICM", "PI#M"
    public static int[] SUMMARY_CODE_POSITIONS_471 =  {172,    181,     290,    284,    236,    230,    150,    386,    344,    338 };
    //                                                "HWAH", "BWAH",  "NICM", "NI#M", "IRCM", "IR#M", "DWAP", "RECM", "PICM", "PI#M"
    public static int[] SUMMARY_CODE_POSITIONS_461 =  {163,    172,     279,    273,    225,    219,    141,    369,    327,    321};



    protected static Double[] parseSummaryfile(String summaryFile) throws FileParseException {
        Double result[] = new Double[SUMMARY_CODE_POSITIONS_470.length];


        // Headers for 4.7.1
        String headerV475 = "@   RUNNO   TRNO R# O# P# CR MODEL... EXNAME.. TNAM..................... FNAM.... WSTA.... SOIL_ID...    SDAT    PDAT    EDAT    ADAT    MDAT    HDAT  DWAP    CWAM    HWAM    HWAH    BWAH  PWAM    HWUM    H#AM    H#UM  HIAM  LAIX  IR#M  IRCM  PRCM  ETCM  EPCM  ESCM  ROCM  DRCM  SWXM  NI#M  NICM  NFXM  NUCM  NLCM  NIAM  CNAM  GNAM N2OEC  PI#M  PICM  PUPC  SPAM  KI#M  KICM  KUPC  SKAM  RECM  ONTAM   ONAM  OPTAM   OPAM   OCTAM    OCAM   CO2EC    DMPPM    DMPEM    DMPTM    DMPIM     YPPM     YPEM     YPTM     YPIM    DPNAM    DPNUM    YPNAM    YPNUM  NDCH TMAXA TMINA SRADA DAYLA   CO2A   PRCP   ETCP   ESCP   EPCP";

        // Headers for 4.7.1
        String headerV471 = "@   RUNNO   TRNO R# O# C# CR MODEL... EXNAME.. TNAM..................... FNAM.... WSTA.... SOIL_ID...    SDAT    PDAT    EDAT    ADAT    MDAT    HDAT  DWAP    CWAM    HWAM    HWAH    BWAH  PWAM    HWUM    H#AM    H#UM  HIAM  LAIX  IR#M  IRCM  PRCM  ETCM  EPCM  ESCM  ROCM  DRCM  SWXM  NI#M  NICM  NFXM  NUCM  NLCM  NIAM  CNAM  GNAM N2OEC  PI#M  PICM  PUPC  SPAM  KI#M  KICM  KUPC  SKAM  RECM  ONTAM   ONAM  OPTAM   OPAM   OCTAM    OCAM   CO2EC    DMPPM    DMPEM    DMPTM    DMPIM     YPPM     YPEM     YPTM     YPIM    DPNAM    DPNUM    YPNAM    YPNUM  NDCH TMAXA TMINA SRADA DAYLA   CO2A   PRCP   ETCP   ESCP   EPCP";
        // Headers for 4.7.0 and before
        String headerV470 = "@   RUNNO   TRNO R# O# C# CR MODEL... EXNAME.. TNAM..................... FNAM.... WSTA.... SOIL_ID...    SDAT    PDAT    EDAT    ADAT    MDAT    HDAT  DWAP    CWAM    HWAM    HWAH    BWAH  PWAM    HWUM    H#AM    H#UM  HIAM  LAIX  IR#M  IRCM  PRCM  ETCM  EPCM  ESCM  ROCM  DRCM  SWXM  NI#M  NICM  NFXM  NUCM  NLCM  NIAM  CNAM  GNAM  PI#M  PICM  PUPC  SPAM  KI#M  KICM  KUPC  SKAM  RECM  ONTAM   ONAM  OPTAM   OPAM   OCTAM    OCAM    DMPPM    DMPEM    DMPTM    DMPIM     YPPM     YPEM     YPTM     YPIM    DPNAM    DPNUM    YPNAM    YPNUM  NDCH TMAXA TMINA SRADA DAYLA   CO2A   PRCP   ETCP   ESCP   EPCP";

        String headerV461 = "@   RUNNO   TRNO R# O# C# CR MODEL... TNAM..................... FNAM.... WSTA.... SOIL_ID...    SDAT    PDAT    EDAT    ADAT    MDAT    HDAT  DWAP    CWAM    HWAM    HWAH    BWAH  PWAM    HWUM  H#AM    H#UM  HIAM  LAIX  IR#M  IRCM  PRCM  ETCM  EPCM  ESCM  ROCM  DRCM  SWXM  NI#M  NICM  NFXM  NUCM  NLCM  NIAM  CNAM  GNAM  PI#M  PICM  PUPC  SPAM  KI#M  KICM  KUPC  SKAM  RECM  ONTAM   ONAM  OPTAM   OPAM   OCTAM    OCAM    DMPPM    DMPEM    DMPTM    DMPIM     YPPM     YPEM     YPTM     YPIM    DPNAM    DPNUM    YPNAM    YPNUM  NDCH TMAXA TMINA SRADA DAYLA   CO2A   PRCP   ETCP   ESCP   EPCP";

        String[] sections = null;
        int[] summaryCodePositions = null;
        if (summaryFile.contains(headerV470)) {
            sections = summaryFile.split(headerV470);
            summaryCodePositions = SUMMARY_CODE_POSITIONS_470;
        } else if (summaryFile.contains(headerV471) ) {
            sections = summaryFile.split(headerV471);
            summaryCodePositions = SUMMARY_CODE_POSITIONS_471;
        } else if (summaryFile.contains(headerV475)){
            sections = summaryFile.split(headerV475);
            summaryCodePositions = SUMMARY_CODE_POSITIONS_471;
        }else if(summaryFile.contains(headerV461)){
            sections = summaryFile.split(headerV461);
            summaryCodePositions = SUMMARY_CODE_POSITIONS_461;
        }else{
            throw new FileParseException("Summary file isn't properly formatted.\nDoes not contain the proper header.\n" + summaryFile + "\n");
        }

        if(sections.length != 2)
            throw new FileParseException("Summary file missing data\n" + summaryFile + "\n");

        String treatments = sections[1];

        // TODO add multiple treatment support
        String treatment1 = treatments.split("\\r?\\n",-1)[1];

        String value;
        for(int i = 0; i < summaryCodePositions.length; i++) {
            value = treatment1.substring(summaryCodePositions[i]);
            value = value.trim();
            value = value.replaceAll(" .*$", "");
            result[i] = Double.parseDouble(value);
        }

        return result;

    }

    /*
     * Soil Inorganic nitrogen balance file
     */
    protected static Double parseSoilInorganicNBalanceFile(String soilInorganicNBalanceFileContents) throws FileParseException {

        Double result;

        String leachingPatternV471 = "!   N leached\\s+\\d+\\.?\\d*";
        String leachingPatternV470 = "!   Leached NO3\\s+\\d+\\.?\\d*";

        Pattern pat470 = Pattern.compile(leachingPatternV470);
        Pattern pat471 = Pattern.compile(leachingPatternV471);

        Matcher matcherV470 = pat470.matcher(soilInorganicNBalanceFileContents);
        Matcher matcherV471 = pat471.matcher(soilInorganicNBalanceFileContents);

        Matcher matcher = null;
        Pattern pat = null;
        String leachedLine;
        if(matcherV470.find()) {
            pat = pat470;
            matcher = matcherV470;
        }else if(matcherV471.find()) {
            pat = pat471;
            matcher = matcherV471;
        }else
            throw new FileParseException("Unable to find leached NO3 line");

        leachedLine = matcher.group(0);

        String doublePattern = "\\d+\\.?\\d*\\s*$";

        pat = Pattern.compile(doublePattern);

        matcher = pat.matcher(leachedLine);

        if(!matcher.find())
            throw new FileParseException("Unable to find leached NO3 value");

        result = Double.parseDouble(matcher.group(0));

        return result;

    }


    private static final String GROW_FILE_HEADER = "@YEAR DOY   DAS   DAP   L#SD   GSTD   LAID   LWAD   SWAD   GWAD   RWAD   VWAD   CWAD   G#AD   GWGD   HIAD   PWAD   P#AD   WSPD   WSGD   NSTD   EWSD  PST1A  PST2A   KSTD   LN%D   SH%D   HIPD   PWDD   PWTD     SLAD   CHTD   CWID   RDPD    RL1D    RL2D    RL3D    RL4D    RL5D    RL6D    RL7D    RL8D    RL9D   CDAD   LDAD   SDAD   SNW0C   SNW1C  DTTD";
    private static final int ROOT_WEIGHT = 10;
    private static final int VEGETATIVE_WEIGHT = 11;
    private static final int GRAIN_WEIGHT = 9;
    private static final int TOPS_WEIGHT = 12;
    private static final int YEAR = 0;
    private static final int DOY = 1;

    /*
     * Plant growth output file
     */
    public static DatedList<PlantGrowthDatum> parsePlantGroFile(String plantGroFileContents) throws FileParseException{
        DatedList<PlantGrowthDatum> result = new DatedList<PlantGrowthDatum>();

        String[] sections = plantGroFileContents.split(GROW_FILE_HEADER);

        if(sections.length != 2)
            throw new FileParseException("Plant Gro File Not Properly formatted. Couldn't find at least one header" +
                    LINE_SEPARATOR + "-----------------" + LINE_SEPARATOR + plantGroFileContents + LINE_SEPARATOR +
                    "-----------------");

        boolean skippedFirst = false;
        for(String section : sections){
            if(!skippedFirst) {
                skippedFirst = true;
                continue;
            }

            for(String line : section.split("\\r?\\n")){
                String[] fields = line.trim().split("\\s+");
                if(fields.length == 0 || (fields.length == 1 && fields[0].trim().equals(""))) continue;
                else if(fields.length != 49)
                    throw new FileParseException("Plant Gro File Not Properly formatted. Expected 49 columns, got "+
                    fields.length);

                Double vegWeight;
                Double grainWeight;
                Double topsWeight;
                Double rootWeight;
                String date;

                try{
                    vegWeight = Double.parseDouble(fields[VEGETATIVE_WEIGHT]);
                    grainWeight = Double.parseDouble(fields[GRAIN_WEIGHT]);
                    topsWeight = Double.parseDouble(fields[TOPS_WEIGHT]);
                    rootWeight = Double.parseDouble(fields[ROOT_WEIGHT]);
                    date = fields[YEAR].substring(2,4) + fields[DOY];
                }catch(NumberFormatException e){
                    throw new FileParseException("Unable to parse one of these doubles: " + LINE_SEPARATOR +
                        fields[VEGETATIVE_WEIGHT] + ", " + fields[GRAIN_WEIGHT] + " " + fields[TOPS_WEIGHT] + " " +
                        fields[ROOT_WEIGHT] + LINE_SEPARATOR + e.getMessage());
                }

                DatedItem<PlantGrowthDatum> item;
                try{
                    item = new DatedItem<PlantGrowthDatum>(date,
                            new PlantGrowthDatum(vegWeight,grainWeight,topsWeight, rootWeight));
                }catch(IllegalStringFormatException e){
                    throw new FileParseException("Bad date given" + LINE_SEPARATOR + e.getLocalizedMessage());
                }

                result.addItem(item);

            }

        }


        return result;

    }

    public static Profile parseProfile(String profileContents) throws FileParseException{
        Profile result = new Profile();

        // MBA C: \DSSAT47 DSCSM047.EXE CSCER047
        // MBA .  DSCSM047.EXE CSCER047

        /*
            (note: fields can have length of zero

            Code
                        SPACE
            Drive
                        SPACE
            Directory
                        SPACE
            File
                        SPACE
            Parameter

        */


        for (String row : profileContents.split("\\r?\\n")){
            if(row.matches("^ *[@!\\*].*$")) continue; // skip comments
            if(row.trim().equals("")) continue; // Skip blank lines


            String[] fields = row.split(" ", -1);

            if(fields.length < 3 || fields.length > 5)
                throw new FileParseException(
                        String.format("Invalid profile file. Should be between 3 and 5 fields long. Got [%s]", row));

            String key = fields[0];
            String drive = fields[1];
            String directory = fields[2];
            String file = null;
            String parameter = null;

            if(fields.length > 3 && ! fields[3].equals("")) {
                file = fields[3];
                if(fields.length > 4 && ! fields[4].equals(""))
                    parameter = fields[4];
            }

            result.addEntry(key, drive, directory, file, parameter);

        }

        return result;

    }


}
