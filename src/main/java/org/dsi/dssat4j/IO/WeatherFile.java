package org.dsi.dssat4j.IO;


import org.dsi.dssat4j.beans.WeatherDate;
import org.dsi.dssat4j.beans.WeatherStation;
import org.dsi.dssat4j.exceptions.IllegalWeatherDataException;

import java.io.*;
import java.util.GregorianCalendar;

public class WeatherFile {

    private WeatherStation station;

    public WeatherFile(String weatherFilePath) throws IOException, IllegalWeatherDataException {
        String fileName = (new File(weatherFilePath)).getName();
        try(BufferedReader br = new BufferedReader(new FileReader(weatherFilePath))) {

            if (fileName.matches(".*\\.[wW][tT][hH]$")) {
                this.station = parseWTHFile(br);
            } else if (fileName.matches(".*\\.[wW][tT][dD]$")) {
                this.station = parseWTDFile(br);
            } else {
                throw new IllegalWeatherDataException("File type of [" + fileName + "] not supported for weather data");
            }

            String[] results = fileName.split("\\.");

            if(results.length != 2){
                throw new IllegalWeatherDataException("Weather file should be in the format ABCD1234.WT[H|D]. Got [" + fileName + "]");
            }

            try {
                this.station.setWeatherStationID(fileName.substring(0, 4));
                this.station.setWeatherStationFileID(fileName.substring(4, 8));
            } catch(StringIndexOutOfBoundsException e){
                throw new IllegalWeatherDataException("Weather file should be in the format ABCD1234.WT[H|D]. Got [" + fileName + "]");
            }

        }
    }

    private WeatherStation parseWTHFile(BufferedReader wthFileReader) throws IOException, IllegalWeatherDataException {

        // Field indices
        final int INSTITUTION = 0;
        final int LATITUDE = 1;
        final int LONGITUDE = 2;
        final int ELEVATION = 3;

        WeatherStation result;

        String line = wthFileReader.readLine();
        // Skip until we find the field site info
        while(line != null &&
                ! line.matches("@ INSI\\s{6}LAT\\s{5}LONG\\s{2}ELEV\\s{3}TAV\\s{3}AMP REFHT WNDHT")){
            line = wthFileReader.readLine();
        }

        if (line == null)
            throw new IllegalWeatherDataException("Expecting the field info header in the weather file.");
        else
            line = wthFileReader.readLine();

        String fields[];
        if(line  == null )
            throw new IllegalWeatherDataException("Expecting station info. Got nuttin'");
        else
            fields = line.trim().split(" +");


        result = parseWeatherData(wthFileReader);

        String fieldName = "???";
        // add the additional double fields
        for(int i = 0; i < fields.length; i++){
            try{
                switch(i){
                    case INSTITUTION:
                        fieldName = "institution";
                        result.setInstitution(fields[INSTITUTION]);
                    case LONGITUDE:
                        fieldName = "longitude";
                        result.setLongitude(Double.parseDouble(fields[LONGITUDE]));
                        break;
                    case LATITUDE:
                        fieldName = "latitude";
                        result.setLatitude(Double.parseDouble(fields[LATITUDE]));
                        break;
                    case ELEVATION:
                        fieldName = "elevation";
                        result.setElevation(Integer.parseInt(fields[ELEVATION]));
                        break;
                    default:
                        break;
                }
            }catch(NumberFormatException e){
                throw new IllegalWeatherDataException("Expected a double in the " + fieldName + " field ");
            }
        }

        return result;

    }

    // TODO Move this function into the parser file
    private WeatherStation parseWTDFile(BufferedReader wtdFilePath) throws IOException, IllegalWeatherDataException{
        return parseWeatherData(wtdFilePath);
    }

    // TODO Move this function into the parser file
    private WeatherStation parseWeatherData(BufferedReader inputBuffer) throws IOException, IllegalWeatherDataException {

        // Field indices
        final int DATE = 0;
        final int SOLAR_RADIATION = 1;
        final int MAX_TEMP = 2;
        final int MIN_TEMP = 3;
        final int RAIN = 4;

        WeatherStation results = new WeatherStation();

        // The first line should be the header
        if((inputBuffer.readLine()) == null) throw new IllegalWeatherDataException("No weather data given.");

        String line;
        String fields[];
        WeatherDate weatherDate;
        Double srad = 0.0;
        Double tmin = 0.0;
        Double tmax = 0.0;
        Double rain = 0.0;
        String dateString = "";

        // For each date of the weather file...
        String fieldName = "";
        while((line = inputBuffer.readLine()) != null){
            fields = line.trim().split(" +");

            // Look at each column of the weather data
            for(int i = 0; i < fields.length; i++){
                try {
                    switch (i) {
                        case DATE:
                            fieldName = "date";
                            dateString = fields[DATE];
                            if(dateString.matches("\\d{7}"))
                                dateString = dateString.substring(2);
                            break;
                        case SOLAR_RADIATION:
                            fieldName = "solar radiation";
                            srad = Double.parseDouble(fields[SOLAR_RADIATION]);
                            break;
                        case MAX_TEMP:
                            fieldName = "maximum temperature";
                            tmax = Double.parseDouble(fields[MAX_TEMP]);
                            break;
                        case MIN_TEMP:
                            fieldName = "minimum temperature";
                            tmin = Double.parseDouble(fields[MIN_TEMP]);
                            break;
                        case RAIN:
                            fieldName = "rain";
                            rain = Double.parseDouble(fields[RAIN]);
                            break;
                        default:
                            break;
                    }
                }catch(NumberFormatException e){
                    throw new IllegalWeatherDataException(e.getMessage() + "\nUnable to parse double field " + fieldName);
                }
            }

            results.addDate(dateString, srad, tmax,tmin,rain);
        }

        return results;

    }

    public WeatherStation getStation(){
        return this.station;
    }

}
