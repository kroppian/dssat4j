package org.dsi.dssat4j.samples;

import java.io.File;
import java.io.IOException;

import org.dsi.dssat4j.IO.Experiment;
import org.dsi.dssat4j.IO.Outcome;
import org.dsi.dssat4j.IO.Parsers;
import org.dsi.dssat4j.beans.ResourceApp;
import org.dsi.dssat4j.beans.Treatment;
import org.dsi.dssat4j.exceptions.FileParseException;
import org.dsi.dssat4j.exceptions.FailedRunException;
import org.dsi.dssat4j.exceptions.IllegalApplicationException;
import org.dsi.dssat4j.beans.IrrigationApps;
import org.dsi.dssat4j.beans.FertilizerApps;
import org.dsi.dssat4j.exceptions.SessionException;
import org.dsi.dssat4j.tools.*;


import java.lang.Thread;


class ParallelExecution {


    public class DssatThread implements Runnable {

        int irrAmount;
        int runid;
        DssatSession sesh;

        DssatThread(DssatSession sesh, int runid, int amount) {
            this.irrAmount = amount;
            this.runid = runid;
            this.sesh = sesh;
        }

        public void run() {
            int yield = runDssat(this.sesh, runid, this.irrAmount);
            System.out.println("The yeild is: " + yield);
        }

    }

    public void runThreads(DssatSession session) {

        int[] irrAmounts = {10, 20, 30, 40, 50, 60};
        int numThread = 8;

        Thread[] t = new Thread[numThread];
        try {
            for (int i = 0; i < numThread; i++) {
                t[i] = new Thread(new DssatThread(session, i, irrAmounts[i % irrAmounts.length]));
                t[i].start();
            }
            for (int i = 0; i < numThread; i++) {
                t[i].join();//wait till finish
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    //   public DssatConfig(String installDir, CropTypes cropType, String instCode, String siteCode, int year, String expNumb){
    public static void main(String[] args) {

        final String S = File.separator;
        File runDir = new File(S + "tmp" + S + "dssat4j");
        File exe = new File(System.getenv("DSSAT_EXE"));

        File profile = new File(System.getenv("DSSAT_PROF"));;

        runDir.mkdir();

        DssatConfig config = null;
        try{
             config = new DssatConfig(runDir, exe, profile);
        }catch(IOException e){
            System.err.println("Bad config files given:");
            System.err.println(e.getMessage());
            System.exit(1);
        }

        DssatSessionFactory seshFact = new DssatSessionFactory();

        DssatSession sesh = null;
        try{
            sesh = seshFact.createSession(config);
        }catch(SessionException e){
            System.err.println("Exception while setting up the session:\n" + e.getMessage());
        }

        ParallelExecution dssat = new ParallelExecution();
        for (int i = 0; i < 200; i++) {
            dssat.runThreads(sesh);
        }
    }

    public static int runDssat(DssatSession sesh, int runId, int irrigationAmount) {

        Experiment exp = null;
        try {
            File expTemplate = new File("src/test/resources/DSSAT471/Maize/UFGA8202.MZX");
            exp = Parsers.parseExpFile(expTemplate);
            IrrigationApps irrData = exp.getIrrigationTreatment(1);
            FertilizerApps fertData = exp.getFertilizerTreatment(1);
            ResourceApp.Method method = ResourceApp.Method.IR004;
            irrData.addIrrApp(1982, 4, 16, method, irrigationAmount);
            exp.setIrrigationLevel(2, irrData);
            exp.setFertilizerLevel(2, fertData);

        } catch (IOException e) {
            System.err.println("Unable to find exp file.");
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (FileParseException e) {
            System.err.println("Unable to parse exp file.");
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (IllegalApplicationException e) {
            System.err.println("Illegal app exception");
            System.err.println(e.getMessage());
            System.exit(1);
        }

        Outcome outcome = null;
        DssatRunner runner = null;
        try{
             runner = sesh.getDssatRunner();
        }catch(SessionException e){
            System.err.println("Run failed while getting runner");
            System.err.println(e.getMessage());
            System.exit(1);
        }

        try {
            outcome = runner.run(exp,4);
        } catch (FailedRunException e) {
            System.err.println("Run failed");
            System.err.println(e.getMessage());
            System.exit(1);
        }

        int yield = outcome.getYield();
        double leached = outcome.getNitroLeaching();

        return yield;

    }

}



