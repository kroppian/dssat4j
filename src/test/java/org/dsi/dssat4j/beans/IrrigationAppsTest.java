package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.beans.ResourceApp.Substance;
import org.dsi.dssat4j.exceptions.IllegalApplicationException;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Created by kroppian on 6/21/2017.
 */
public class IrrigationAppsTest {

    private static ResourceApp.Method METHOD = ResourceApp.Method.IR004;

    @Test
    public void testAddData() {

        IrrigationApps data = new IrrigationApps();
        try {
            data.addIrrApp(1992, 3, 24, METHOD, 55);
            data.addIrrApp(1992, 1, 12, METHOD, 10);
            data.addIrrApp(2003, 7, 22, METHOD, 99);
        } catch (IllegalApplicationException e) {
            System.out.println(e.getMessage());
            fail("Unable to add irrigation applications. Failing.");
        }

        assertEquals(3, data.size());
        assertEquals(2, data.getByDate(1992).size());
        assertEquals(1, data.getByDate(1992, 3, 24).size());

        ResourceApp datum = data.getByDate(2003, 7, 22).getFirst();
        assertEquals(2003, datum.getYear());
        assertEquals(7, datum.getMonth());
        assertEquals(22, datum.getDayOfMonth());

        assertEquals(99, datum.getAmount());

    }

    @Test
    public void testNestedFilters() {

        IrrigationApps data = new IrrigationApps();
        try {
            data.addIrrApp(1992, 3, 24, METHOD, 55);
            data.addIrrApp(1992, 1, 12, METHOD, 10);
            data.addIrrApp(2003, 7, 22, METHOD, 99);
            data.addIrrApp(2003, 8, 7, METHOD, 30);
        } catch (IllegalApplicationException e) {
            System.out.println(e.getMessage());
            fail("Unable to add irrigation applications. Failing.");
        }

        assertEquals(4, data.getBySubstance(Substance.WATER).size());
        assertEquals(2, data.getBySubstance(Substance.WATER).getByDate(2003).size());
        assertEquals(99, data
                .getBySubstance(Substance.WATER)
                .getByDate(2003, 7)
                .getFirst().getAmount());

    }

    @Test
    public void testNoDuplicateApplications() {

        IrrigationApps data = new IrrigationApps();
        try {
            data.addIrrApp(1992, 3, 24, METHOD, 55);
            data.addIrrApp(1992, 1, 12, METHOD, 10);
            data.addIrrApp(2003, 7, 22, METHOD, 99);
            data.addIrrApp(1992, 3, 24, METHOD,  30);
            fail("Application exception not thrown");
        } catch (IllegalApplicationException e) {
        }


    }

}

