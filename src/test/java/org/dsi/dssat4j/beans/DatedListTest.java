package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.beans.DatedItem;
import org.dsi.dssat4j.beans.DatedList;
import org.dsi.dssat4j.exceptions.IllegalStringFormatException;
import org.junit.Test;
import static org.junit.Assert.*;

public class DatedListTest {

    @Test
    public void testIrrigationApplied470(){

        DatedList<Integer> list = new DatedList<Integer>();

        try{
            list.addItem("82059",2);
            list.addItem("82070",3);
            list.addItem("82033",1);
            list.addItem("82120",4);
        }catch(IllegalStringFormatException e){

        }

        list.sort();

        assertEquals(1, list.getItem(0).getItem());
        assertEquals(2, list.getItem(1).getItem());
        assertEquals(3, list.getItem(2).getItem());
        assertEquals(4, list.getItem(3).getItem());


    }

}
