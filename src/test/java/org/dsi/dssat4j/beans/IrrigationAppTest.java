package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.beans.ResourceApp.Substance;

import static org.junit.Assert.*;

import org.junit.Test;
import org.dsi.dssat4j.exceptions.*;

/**
 * Created by kroppian on 6/20/2017.
 */
public class IrrigationAppTest {

    @Test
    public void testConstructor() {

        ResourceApp datum = new ResourceApp(2017, 11, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        assertEquals(
                datum.getYear(),
                2017);

        assertEquals(
                datum.getMonth(),
                11);

        assertEquals(
                datum.getDayOfMonth(),
                5);

        assertEquals(
                datum.getAmount(), 12);

        assertEquals(
                datum.getSubstance(), Substance.WATER);

        assertEquals(
                7,datum.getDepth()
        );

    }

    @Test
    public void testDateSetter() {

        ResourceApp datum = new ResourceApp(2017, 1, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        datum.setDate(2011, 12, 4);

        assertEquals(
                2011,
                datum.getYear());

        assertEquals(
                12,
                datum.getMonth());

        assertEquals(
                4,
                datum.getDayOfMonth());

        assertEquals(
                7,
                datum.getDepth());
    }

    @Test
    public void testYearSetter() {

        ResourceApp datum = new ResourceApp(2017, 1, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        datum.setYear(2001);

        assertEquals(
                2001,
                datum.getYear());

        assertEquals(
                1,
                datum.getMonth());

        assertEquals(
                5,
                datum.getDayOfMonth());

        assertEquals(
                7,
                datum.getDepth());

    }

    @Test
    public void testMonthSetter() {

        ResourceApp datum = new ResourceApp(2017, 1, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        datum.setMonth(12);

        assertEquals(
                2017,
                datum.getYear());

        assertEquals(
                12,
                datum.getMonth());

        assertEquals(
                5,
                datum.getDayOfMonth());

        assertEquals(
                7,
                datum.getDepth());

    }

    @Test
    public void testDayOfMonthSetter() {

        ResourceApp datum = new ResourceApp(2017, 3, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        datum.setDayOfMonth(30);

        assertEquals(
                2017,
                datum.getYear());

        assertEquals(
                3,
                datum.getMonth());

        assertEquals(
                30,
                datum.getDayOfMonth());

        assertEquals(
                7,
                datum.getDepth());
    }

    @Test
    public void testAmountSetter() {

        ResourceApp datum = new ResourceApp(2017, 1, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        datum.setApplicationAmount(302);
        assertEquals(
                datum.getAmount(),
                302);

    }

    @Test
    public void testSubstanceSetter() {

        ResourceApp datum = new ResourceApp(2017, 0, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

    /*datum.setSubstance(Substance.NITROGEN);
    assertEquals(
        datum.getSubstance(),
        Substance.NITROGEN);*/

    }

    @Test
    public void testDateSetterYYDDD() {

        ResourceApp datum = new ResourceApp(2017, 0, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        try {
            datum.setDate("92262");
        } catch (IllegalStringFormatException e) {
            System.err.println(e.getMessage());
            fail("setDate failed to parse the given date properly");
        }

        assertEquals(
                datum.getYear(),
                1992);

        assertEquals(
                datum.getMonth(),
                9);

        assertEquals(
                datum.getDayOfMonth(),
                18);

    }

    @Test
    public void testDateSetterYYDDDLowerDayBound() {

        ResourceApp datum = new ResourceApp(2017, 0, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        try {
            datum.setDate("04001");
        } catch (IllegalStringFormatException e) {
            System.err.println(e.getMessage());
            fail("setDate failed to parse the given date properly");
        }

        assertEquals(
                datum.getYear(),
                2004);

        assertEquals(
                datum.getMonth(),
                1);

        assertEquals(
                datum.getDayOfMonth(),
                1);

    }

    @Test
    public void testDateSetterYYDDDLowerUpperBound() {

        ResourceApp datum = new ResourceApp(2017, 0, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        try {
            datum.setDate("89365");
        } catch (IllegalStringFormatException e) {
            System.err.println(e.getMessage());
            fail("setDate failed to parse the given date properly");
        }

        assertEquals(
                datum.getYear(),
                1989);

        assertEquals(
                datum.getMonth(),
                12);

        assertEquals(
                datum.getDayOfMonth(),
                31);

    }

    @Test
    public void testDateSetterYYDDDLowerUpperBoundLeapYear() {

        ResourceApp datum = new ResourceApp(2017, 0, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        try {
            datum.setDate("88366");
        } catch (IllegalStringFormatException e) {
            System.err.println(e.getMessage());
            fail("setDate failed to parse the given date properly");
        }

        assertEquals(
                datum.getYear(),
                1988);

        assertEquals(
                datum.getMonth(),
                12);

        assertEquals(
                datum.getDayOfMonth(),
                31);

    }

    @Test
    public void testDateSetterYYDDDYearUpper() {

        ResourceApp datum = new ResourceApp(2017, 0, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        try {
            datum.setDate("50193");
        } catch (IllegalStringFormatException e) {
            System.err.println(e.getMessage());
            fail("setDate failed to parse the given date properly");
        }

        assertEquals(2050, datum.getYear());

        assertEquals(7, datum.getMonth());

        assertEquals(12, datum.getDayOfMonth());

    }

    @Test
    public void testDateSetterYYDDDYearLower() {

        ResourceApp datum = new ResourceApp(2017, 0, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        try {
            datum.setDate("51297");
        } catch (IllegalStringFormatException e) {
            System.err.println(e.getMessage());
            fail("setDate failed to parse the given date properly");
        }

        assertEquals(1951, datum.getYear());

        assertEquals(10, datum.getMonth());

        assertEquals(24, datum.getDayOfMonth());

    }

    @Test
    public void testDepthSetter(){

        ResourceApp datum = new ResourceApp(2017, 0, 5, Substance.WATER, ResourceApp.Method.IR004, 12, 7);

        datum.setDepth(11);

        assertEquals(11, datum.getDepth());

    }


}

