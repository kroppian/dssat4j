package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.IO.Outcome;
import org.dsi.dssat4j.beans.ResourceApp.Substance;
import org.dsi.dssat4j.exceptions.FileParseException;
import org.dsi.dssat4j.exceptions.IllegalApplicationException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by kroppian on 7/11/2017.
 */
public class EconomicsTest {

    File OUTPUT_PATH = new File("src/test/resources/DSSAT470/Maize/");

    Outcome outcome;

    private Outcome readOutcomeFile(File outputPath){
        Outcome outcome = null;
        try{
            outcome = new Outcome(outputPath);
        }catch(IOException e){
            System.err.println(e.getMessage());
            fail("IOException unexpected");
        }catch(FileParseException e){
            System.err.println(e.getMessage());
            fail("File Parse Exception unexpected");
        }
        return outcome;
    }

    @Test
    public void testAverageData() {
        outcome = readOutcomeFile(OUTPUT_PATH);
        Economics economics = new Economics();

        economics.setGrainPrice(1, 10);
        economics.setPriceOfByProduct(1, 5);
        economics.setBaseProductionCost(1, 3000);
        economics.setNitrogenFertilizerCost(1, 20);
        economics.setCostPerNitrogenFertilizerApplication(1, 2);
        economics.setIrrigationCost(1,  19);
        economics.setCostPerIrrApplication(1, 3);
        economics.setSeedCost(1, 0);
        economics.setOrganicAmendments(1, 2);
        economics.setPhosCost(1, 13);
        economics.setCostPerPhosApplied(1, 4);

        double result = economics.calculateGrossMargin(1, outcome);

        assertEquals(-808902, result,0.01);


    }





}

