package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.beans.ResourceApp.Substance;
import org.dsi.dssat4j.exceptions.IllegalApplicationException;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Created by kroppian on 7/11/2017.
 */
public class FertilizerAppsTest {

    ResourceApp.Method METHOD = ResourceApp.Method.AP004;

    @Test
    public void testAddData() {

        FertilizerApps data = new FertilizerApps();
        try {
            data.addNitroApp(1992, 3, 24, METHOD,55, 7);
            data.addNitroApp(1991, 3, 2, METHOD, 23, 12);
            data.addPhosApp(1992, 1, 12, METHOD, 10, 5);
            data.addPotApp(2003, 7, 22, METHOD, 99, 2);
            data.addPotApp(1983, 4, 2, METHOD, 120,1);
            data.addPotApp(2007, 2, 21, METHOD, 101,3);
        } catch (IllegalApplicationException e) {
            System.out.println(e.getMessage());
            fail("Unable to add irrigation applications. Failing.");
        }

        assertEquals(6, data.size());
        assertEquals(2, data.getByDate(1992).size());
        assertEquals(1, data.getByDate(1992, 3, 24).size());

        assertEquals(3, data.getBySubstance(Substance.POTASSIUM).size());
        assertEquals(2, data.getBySubstance(Substance.NITROGEN).size());
        assertEquals(1, data.getBySubstance(Substance.PHOSPHORUS).size());


        ResourceApp datum = data.getByDate(2003, 7, 22).getFirst();
        assertEquals(2003, datum.getYear());
        assertEquals(7, datum.getMonth());
        assertEquals(22, datum.getDayOfMonth());

        assertEquals(99, datum.getAmount());
        assertEquals(2, datum.getDepth());

    }

    @Test
    public void testNoDuplicateApplications() {

        FertilizerApps data = new FertilizerApps();
        try {
            data.addNitroApp(1992, 3, 24, METHOD, 55, 7);
            data.addNitroApp(1992, 1, 12, METHOD, 10, 7);
            data.addNitroApp(2003, 7, 22, METHOD, 99, 7);
            data.addNitroApp(1992, 3, 24, METHOD, 30, 7);
            fail("Application exception not thrown");
        } catch (IllegalApplicationException e) {
        }


        data = new FertilizerApps();
        try {
            data.addPotApp(1992, 3, 24, METHOD, 55, 8);
            data.addPotApp(1992, 1, 12, METHOD, 10, 8);
            data.addPotApp(2003, 7, 22, METHOD, 99, 8);
            data.addPotApp(1992, 3, 24, METHOD, 30, 8);
            fail("Application exception not thrown");
        } catch (IllegalApplicationException e) {
        }


        data = new FertilizerApps();
        try {
            data.addPhosApp(1992, 3, 24, METHOD, 55, 9);
            data.addPhosApp(1992, 1, 12, METHOD, 10, 9);
            data.addPhosApp(2003, 7, 22, METHOD, 99, 9);
            data.addPhosApp(1992, 3, 24, METHOD, 30, 9);
            fail("Application exception not thrown");
        } catch (IllegalApplicationException e) {
        }

    }


    @Test
    public void testCompoundApplication() {

    /* On each of the following days, we are goign 
     * to apply equal parts Nitrogen, Phosphorus and 
     * Potassium
     */
        FertilizerApps data = new FertilizerApps();
        try {
            data.addNitroApp(1992, 3, 24, METHOD, 55, 11);
            data.addNitroApp(1992, 1, 12, METHOD, 10, 11);
            data.addNitroApp(2003, 7, 22, METHOD, 99, 11);

            data.addPotApp(1992, 3, 24, METHOD, 55, 11);
            data.addPotApp(1992, 1, 12, METHOD, 10, 11);
            data.addPotApp(2003, 7, 22, METHOD, 99, 11);

            data.addPhosApp(1992, 3, 24, METHOD, 55, 11);
            data.addPhosApp(1992, 1, 12, METHOD, 10, 11);
            data.addPhosApp(2003, 7, 22, METHOD, 99, 11);

        } catch (IllegalApplicationException e) {
            fail("IllegalApplicationException unnecessarily thrown");
        }

    }


}

