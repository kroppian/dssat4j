package org.dsi.dssat4j.beans;

import org.dsi.dssat4j.beans.ResourceApp.Substance;
import org.dsi.dssat4j.exceptions.IllegalApplicationException;

import static org.junit.Assert.*;

import org.junit.Test;
import org.dsi.dssat4j.exceptions.*;

/**
 * Created by kroppian on 7/11/2017.
 */
public class FertilizerAppTest {

    ResourceApp.Method METHOD = ResourceApp.Method.AP004;

    @Test
    public void testConstructor() {
    /* Test nitrogen */
        ResourceApp application = new ResourceApp(1993, 6, 9, Substance.NITROGEN, METHOD, 32, 7);

        assertEquals(1993, application.getYear());
        assertEquals(6, application.getMonth());
        assertEquals(9, application.getDayOfMonth());
        assertEquals(32, application.getAmount());
        assertEquals(7, application.getDepth());
        assertEquals(application.getSubstance(), Substance.NITROGEN);

    /* Test phosphorus */
        application = null;
        application = new ResourceApp(2000, 1, 1, Substance.PHOSPHORUS, METHOD, 202,22);

        assertEquals(2000, application.getYear());
        assertEquals(1, application.getMonth());
        assertEquals(1, application.getDayOfMonth());
        assertEquals(202, application.getAmount());
        assertEquals(22, application.getDepth());
        assertEquals(application.getSubstance(), Substance.PHOSPHORUS);

    /* Test potassium */
        application = new ResourceApp(1975, 12, 31, Substance.POTASSIUM, METHOD, 0, 23);

        assertEquals(1975, application.getYear());
        assertEquals(12, application.getMonth());
        assertEquals(31, application.getDayOfMonth());
        assertEquals(0, application.getAmount());
        assertEquals(23, application.getDepth());
        assertEquals(application.getSubstance(), Substance.POTASSIUM);

    }


    @Test
    public void testSetters() {

        ResourceApp application = new ResourceApp(1993, 6, 9, Substance.NITROGEN, METHOD, 32, 11);

        application.setYear(2017);
        application.setMonth(3);
        application.setDayOfMonth(5);
        application.setApplicationAmount(99);

        assertEquals(2017, application.getYear());
        assertEquals(3, application.getMonth());
        assertEquals(5, application.getDayOfMonth());
        assertEquals(99, application.getAmount());
        assertEquals(11, application.getDepth());
        assertEquals(application.getSubstance(), Substance.NITROGEN);

    }

}

