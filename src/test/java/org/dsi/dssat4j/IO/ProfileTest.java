package org.dsi.dssat4j.IO;

import org.dsi.dssat4j.beans.Profile;
import org.dsi.dssat4j.exceptions.FileParseException;
import org.junit.Test;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;


public class ProfileTest {

    private File WIN_PROF_FILE = new File("src/test/resources/DSSAT471/DSSATPRO.v47");
    private File LIN_PROF_FILE = new File("src/test/resources/DSSAT471/DSSATPRO.L47");

    @Test
    public void testParseProfileWindows() {
        Charset charset = null;
        String origProf = null;

        // Read the original file
        try{
            origProf = FileUtils.readFileToString(WIN_PROF_FILE,charset);
        }catch(IOException e){
            fail("IO Exception while reading profile:\n" + e.getMessage());
        }

        // Parse the file
        Profile prof = null;
        try{
            prof = Parsers.parseProfile(origProf);
        }catch(FileParseException e){
            fail("File parse exception while reading profile:\n" + e.getMessage());
        }

        // Try to reconstruct profile
        String newProf = Formatting.formatProfile(prof);

        // Remove all whitespace lines and comments from the original
        origProf = origProf.replaceAll( "[\\*!@].*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/", "" );
        // Remove empty lines
        origProf = origProf.replaceAll("(?m)^[ \t]*\r?\n", "");
        // Remove rando whitespace
        origProf = origProf.replaceAll(" +", " ");

        assertEquals(origProf, newProf);


    }

    @Test
    public void testParseProfileLinux() {
        Charset charset = null;
        String origProf = null;

        // Read the original file
        try{
            origProf = FileUtils.readFileToString(LIN_PROF_FILE,charset);
        }catch(IOException e){
            fail("IO Exception while reading profile:\n" + e.getMessage());
        }

        // Parse the file
        Profile prof = null;
        try{
            prof = Parsers.parseProfile(origProf);
        }catch(FileParseException e){
            fail("File parse exception while reading profile:\n" + e.getMessage());
        }

        // Try to reconstruct profile
        String newProf = Formatting.formatProfile(prof);

        // Remove all whitespace lines and comments from the original
        origProf = origProf.replaceAll( "[\\*!@].*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/", "" );
        // Remove empty lines
        origProf = origProf.replaceAll("(?m)^[ \t]*\r?\n", "");
        // Remove whitespace before EOL
        origProf = origProf.replaceAll("\\s+(\\r|\\n)","\r\n");


        assertEquals(origProf, newProf);


    }


}
