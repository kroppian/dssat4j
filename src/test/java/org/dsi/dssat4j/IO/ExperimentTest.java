package org.dsi.dssat4j.IO;

import static org.junit.Assert.*;

import org.dsi.dssat4j.beans.Treatment;
import org.dsi.dssat4j.tools.DssatConfig;
import org.dsi.dssat4j.tools.TestTools;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;
import org.dsi.dssat4j.exceptions.FileParseException;
import org.dsi.dssat4j.beans.ResourceApps;
import org.dsi.dssat4j.beans.IrrigationApps;
import org.dsi.dssat4j.beans.FertilizerApps;
import org.dsi.dssat4j.beans.ResourceApp.Substance;

public class ExperimentTest {

    private final String S = File.separator;

    private final File EXP_FILE = new File("src" + S + "test" + S + "resources" + S + "DSSAT471" + S + "Maize" + S + "UFGA8201.MZX");
    private final File EXP_FILE_2 = new File("src" + S + "test" + S + "resources" + S + "DSSAT471" + S + "Maize" + S + "UFGA8202.MZX");
    private final File PROFILE = new File("src/test/resources/DSSAT471/DSSATPRO.v47");
    // TODO put this in an environment variable
    //private final File EXE_FILE = new File("C:\\DSSAT47\\dscsm047.exe") ;
    private final File EXE_FILE = new File(System.getenv("DSSAT_EXE")) ;
    private final File RUN_DIR = new File(S + "tmp" + S + "dssat4j");

    private DssatConfig config;

    private final TestTools tools = new TestTools();

    public ExperimentTest() {
        RUN_DIR.mkdir();
        this.config = null;
        try{
            this.config = new DssatConfig(RUN_DIR, EXE_FILE, PROFILE);
        }catch(IOException e){
            fail("Bad config files: \n" + e.getMessage());
        }
    }

    private File createTestFile(File inFile) throws IOException{
        File result = new File(inFile.getParent() + S +  "TEMP_" + inFile.getName());
        FileUtils.forceDeleteOnExit(result);
        return result;
    }

    /*
     * Test methods
     */
    @Test
    public void testReadFile() {
        Experiment exp = null;
        try {
            exp = Parsers.parseExpFile(EXP_FILE);
        } catch (IOException e) {
            System.err.println("Can't read file");
            System.err.println(e.getMessage());
            fail("File cannot be read");
        } catch (FileParseException e) {
            System.err.println("Test file is in the incorrect format.");
            fail("File cannot be parsed");
        }

    }

    @Test
    public void testParseIrrigationData() {

        Experiment exp = null;
        try {
            exp = Parsers.parseExpFile(EXP_FILE);
        } catch (IOException e) {
            System.err.println("Can't read file");
            System.err.println(e.getMessage());
            fail("File cannot be read");
        } catch (FileParseException e) {
            System.err.println("Test file is in the incorrect format.");
            System.err.println(e.getMessage());
            fail("File cannot be parsed");
        }

        ResourceApps data = exp.getIrrigationTreatment(1);
        assertEquals(13, data.getByDate(1982, 3, 4).getFirst().getAmount());
        assertEquals(10, data.getByDate(1982, 3, 18).getFirst().getAmount());
        assertEquals(25, data.getByDate(1982, 5, 2).getFirst().getAmount());
        assertEquals(19, data.getByDate(1982, 5, 14).getFirst().getAmount());

    }

    @Test
    public void testParseFertilizerData() {

        Experiment exp = null;
        try {
            exp = Parsers.parseExpFile(EXP_FILE);
        } catch (IOException e) {
            System.err.println("Can't read file");
            System.err.println(e.getMessage());
            fail("File cannot be read");
        } catch (FileParseException e) {
            System.err.println("Test file is in the incorrect format.");
            System.err.println(e.getMessage());
            fail("File cannot be parsed");
        }

        ResourceApps data = exp.getFertilizerTreatment(1);
        assertEquals(27, data.getByDate(1982, 4, 7).getBySubstance(Substance.NITROGEN).getFirst().getAmount());
        assertEquals(35, data.getByDate(1982, 4, 12).getBySubstance(Substance.PHOSPHORUS).getFirst().getAmount());
        assertEquals(54, data.getByDate(1982, 5, 17).getBySubstance(Substance.POTASSIUM).getFirst().getAmount());

    }


    @Test
    public void testPrintingIrrigationData() {

        String expected =
                "*IRRIGATION AND WATER MANAGEMENT\r\n" +
                        "@I  EFIR  IDEP  ITHR  IEPT  IOFF  IAME  IAMT IRNAME\r\n" +
                        " 1     1   -99   -99   -99   -99   -99   -99    -99\r\n" +
                        "@I IDATE  IROP IRVAL\r\n" +
                        " 1 82063 IR001    13\r\n" +
                        " 1 82077 IR001    10\r\n" +
                        " 1 82094 IR001    10\r\n" +
                        " 1 82107 IR001    13\r\n" +
                        " 1 82111 IR001    18\r\n" +
                        " 1 82122 IR001    25\r\n" +
                        " 1 82126 IR001    25\r\n" +
                        " 1 82129 IR001    13\r\n" +
                        " 1 82132 IR001    15\r\n" +
                        " 1 82134 IR001    19\r\n" +
                        " 1 82137 IR001    20\r\n" +
                        " 1 82141 IR001    20\r\n" +
                        " 1 82148 IR001    15\r\n" +
                        " 1 82158 IR001    19\r\n" +
                        " 1 82161 IR001     4\r\n" +
                        " 1 82162 IR001    25\r\n";

        Experiment exp = null;
        try {
            exp = Parsers.parseExpFile(EXP_FILE);
        } catch (IOException e) {
            System.err.println("Can't read file");
            System.err.println(e.getMessage());
            fail("File cannot be read");
        } catch (FileParseException e) {
            System.err.println("Test file is in the incorrect format.");
            fail("File cannot be parsed");
        }

        ResourceApps data = exp.getIrrigationTreatment(1);
        String result = Formatting.formatEXPFileIrrigation(exp.getIrrigationTreatments(), exp.getIrrManagement());
        assertEquals(expected, result);

    }


    @Test
    public void testSaveToFile() {

        Experiment exp = null;
        try {
            exp = Parsers.parseExpFile(EXP_FILE);
        } catch (IOException e) {
            System.err.println("Can't read file");
            System.err.println(e.getMessage());
            fail("File cannot be read");
        } catch (FileParseException e) {
            System.err.println("Test file is in the incorrect format.");
            fail("File cannot be parsed");
        }

        File tempFile = null;
        Charset charset = null;
        try {
            tempFile = createTestFile(EXP_FILE);
            FileUtils.writeStringToFile(tempFile, Formatting.formatExpFile(exp), charset);
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            fail("Cannot write to file");
        }

        try {
            FileUtils.copyFileToDirectory(EXP_FILE, new File("/tmp/"));
            FileUtils.copyFileToDirectory(tempFile, new File("/tmp/"));
            assertEquals("Files do not match",
                    FileUtils.readFileToString(EXP_FILE,charset).replaceAll("\\s+$","").replaceAll("\\r",""),
                    FileUtils.readFileToString(tempFile,charset).replaceAll("\\s+$","").replaceAll("\\r",""));
        } catch (IOException e) {
            System.err.println(e.getMessage());
            fail("Can't read test files");
        }

    }


    @Test
    public void testSaveToFile2() {

        Experiment exp = null;
        try {
            exp = Parsers.parseExpFile(EXP_FILE_2);
        } catch (IOException e) {
            System.err.println("Can't read file");
            System.err.println(e.getMessage());
            fail("File cannot be read");
        } catch (FileParseException e) {
            System.err.println("Test file is in the incorrect format.");
            fail("File cannot be parsed");
        }

        File tempFile = null;
        Charset charset = null;
        try {
            tempFile = createTestFile(EXP_FILE_2);
            FileUtils.writeStringToFile(tempFile, Formatting.formatExpFile(exp), charset);
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            fail("Cannot write to file");
        }

        try {
            FileUtils.copyFileToDirectory(EXP_FILE_2, new File("/tmp/"));
            FileUtils.copyFileToDirectory(tempFile, new File("/tmp/"));
            assertEquals("Files do not match",
                    FileUtils.readFileToString(EXP_FILE_2,charset).replaceAll("\\s+$","").replaceAll("\\r",""),
                    FileUtils.readFileToString(tempFile,charset).replaceAll("\\s+$","").replaceAll("\\r",""));
        } catch (IOException e) {
            System.err.println(e.getMessage());
            fail("Can't read test files");
        }

    }



    @Test
    public void testUpdateFile() {

        Experiment exp = null;
        try {
            exp = Parsers.parseExpFile(EXP_FILE);
        } catch (IOException e) {
            System.err.println("Can't read file");
            System.err.println(e.getMessage());
            fail("File cannot be read");
        } catch (FileParseException e) {
            System.err.println("File " + EXP_FILE.getAbsolutePath() + " is in the incorrect format.");
            System.err.println(e.getMessage());
            fail("File cannot be parsed");
        }

        IrrigationApps irrApps = exp.getIrrigationTreatment(1);

        irrApps.removeAll(irrApps.getByDate(1982, 1));
        irrApps.removeAll(irrApps.getByDate(1982, 2));
        irrApps.removeAll(irrApps.getByDate(1982, 3));
        irrApps.removeAll(irrApps.getByDate(1982, 4));
        irrApps.removeAll(irrApps.getByDate(1982, 5));

        FertilizerApps fertData = exp.getFertilizerTreatment(1);

        fertData.removeAll(fertData.getByDate(1982, 4, 7));
        fertData.getByDate(1982, 4, 12).getFirst().setApplicationAmount(19);

        Treatment treatment = new Treatment();
        treatment.setTreatmentNumber(1);
        treatment.setIrrigationLevel(1);
        treatment.setFertilizerLevel(1);
        exp.setTreatment(treatment);
        exp.setIrrigationLevel(1, irrApps);
        exp.setFertilizerLevel(1, fertData);

        try {
            exp = Parsers.parseExpFile(EXP_FILE);
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            fail("Cannot write to file");
        } catch (FileParseException e) {
            System.err.println("Test file is in the incorrect format.");
            fail("File cannot be parsed");
        }


        DssatConfig tempFileConf = this.config;

        Experiment exp2 = null;
        File tempFile = null;
        try {
            createTestFile(EXP_FILE);
            exp2 = Parsers.parseExpFile(EXP_FILE);
        } catch (IOException e) {
            System.err.println("Can't read file");
            System.err.println(e.getMessage());
            fail("File cannot be read");
        } catch (FileParseException e) {
            System.err.println("Test file " + tempFile.getAbsolutePath() + " is in the incorrect format.");
            System.err.println(e.getMessage());
            fail("File cannot be parsed");
        }

        ResourceApps data2 = exp2.getIrrigationTreatment(1);

        // For irrigation
        assertEquals(19, data2.getByDate(1982, 6, 7).getFirst().getAmount());
        assertEquals(4, data2.getByDate(1982, 6, 10).getFirst().getAmount());
        assertEquals(25, data2.getByDate(1982, 6, 11).getFirst().getAmount());
        assertEquals(16, data2.size());

        // For fertilizer
        FertilizerApps fertData2 = exp2.getFertilizerTreatment(1);
        assertEquals(3, fertData2.size());
        assertEquals(27, fertData2.getByDate(1982, 4, 7).getFirst().getAmount());

    }

    // TODO test run

}



