package org.dsi.dssat4j.IO;
import org.apache.commons.io.FileUtils;
import org.dsi.dssat4j.beans.WeatherDate;
import org.dsi.dssat4j.beans.WeatherStation;
import org.dsi.dssat4j.exceptions.IllegalWeatherDataException;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.Vector;

import static java.nio.charset.Charset.forName;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class WeatherFileTest {

    double delta = 0.001;
    final String WTH_TEST_FILE_NAME = "./src/test/resources/DSSAT471/Weather/UFGA8201.WTH";
    final String WTD_TEST_FILE_NAME = "./src/test/resources/DSSAT471/Weather/UFGA0001.WTD";

    final String WTH_TEST_TEMP_FILE_NAME = "./src/test/resources/DSSAT471/Weather/TEMP0001.WTH";

    private WeatherFile getWeatherFile(String fileName) {

        WeatherFile file = null;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            try {
                file = new WeatherFile(fileName);
            } catch (IOException io) {
                fail("Test file " + fileName + " not found");
            } catch (IllegalWeatherDataException e) {
                fail("Test file not properly formatted\n" + e.getMessage());
            }
        } catch (FileNotFoundException e) {
            fail("File " + fileName + " not found.");
        } catch (IOException e) {
            fail("Error reading file " + fileName);
        }
        return file;
    }


    @Test
    public void testReadWTHFile() {
        WeatherFile file = getWeatherFile(WTH_TEST_FILE_NAME);
        WeatherStation station;

        station = file.getStation();

        // Comparison WeatherStation
        WeatherStation comparison = new WeatherStation();
        comparison.setElevation(10);
        comparison.setInstitution("UFGA");
        comparison.setLatitude(29.630);
        comparison.setLongitude(-82.370);

        // Verify weather station information
        assertTrue(station.getInstitution().equals(comparison.getInstitution()));
        assertTrue(station.getElevation().equals(comparison.getElevation()));
        assertTrue(station.getLatitude().equals(comparison.getLatitude()));
        assertTrue(station.getLongitude().equals(comparison.getLongitude()));

        // Verify maximum temperatures
        Vector<Double> weatherDates = new Vector<>();
        for (WeatherDate date : station) {
            weatherDates.add(date.getMaxTemperature());
        }

        Double[] arr = {24.4, 22.2, 27.8, 26.1, 17.2, 25.0, 26.7, 22.8, 16.7, 14.4, 14.4, 10.0, 20.0, 18.9, 12.2, 19.4};
        ArrayList<Double> arrayList = new ArrayList<>(Arrays.asList(arr));
        Vector<Double> weatherDatesComparison = new Vector<>();
        weatherDatesComparison.addAll(arrayList);

        for (int i = 0; i < 16; i++) {
            assertEquals("Maximum temperatures don't match.", weatherDates.get(i), weatherDatesComparison.get(i));
        }

        // Verify solar radiation
        Vector<Double> radiationDates = new Vector<>();
        for (WeatherDate date : station) {
            radiationDates.add(date.getSolarRadiation());
        }

        Double[] arr2 = {5.9, 7.0, 9.0, 3.1, 12.9, 11.4, 8.5, 3.0, 14.4, 12.0, 15.0, 10.8, 4.8, 6.2, 14.4, 12.6};
        ArrayList<Double> arrayList2 = new ArrayList<>(Arrays.asList(arr2));
        Vector<Double> radiationDatesComparison = new Vector<>();
        radiationDatesComparison.addAll(arrayList2);

        for (int i = 0; i < 16; i++) {
            assertEquals("Solar radiations don't match.", radiationDates.get(i), radiationDatesComparison.get(i));
        }

        // Verify minimum temperatures
        Vector<Double> tempDates = new Vector<>();
        for (WeatherDate date : station) {
            tempDates.add(date.getMinTemperature());
        }

        Double[] arr3 = {15.6, 15.0, 17.2, 15.0, 2.2, 4.4, 11.7, 14.4, 8.9, 1.1, -6.7, -7.8, 3.9, 6.1, -3.3, -1.7};
        ArrayList<Double> arrayList3 = new ArrayList<>(Arrays.asList(arr3));
        Vector<Double> tempDatesComparison = new Vector<>();
        tempDatesComparison.addAll(arrayList3);

        for (int i = 0; i < 16; i++) {
            assertEquals("Minimum temperatures don't match.", tempDatesComparison.get(i), tempDates.get(i));
        }

        // Verify precipitation
        Vector<Double> rainDates = new Vector<>();
        for (WeatherDate date : station) {
            rainDates.add(date.getPrecipitaiton());
        }

        Double[] arr4 = {19.0, 0.0, 0.0, 9.4, 0.0, 0.0, 0.0, 5.3, 0.0, 0.0, 0.0, 0.0, 19.8, 81.0, 0.0, 0.0};
        ArrayList<Double> arrayList4 = new ArrayList<>(Arrays.asList(arr4));
        Vector<Double> rainDatesComparison = new Vector<>();
        rainDatesComparison.addAll(arrayList4);

        for (int i = 0; i < 16; i++) {
            assertEquals("Precipitation values don't match.", rainDates.get(i), rainDatesComparison.get(i));
        }

    }

    @Test
    public void testReadWTDFile() {

        WeatherFile file = getWeatherFile(WTD_TEST_FILE_NAME);
        WeatherStation station;
        station = file.getStation();

        // Verify maximum temperatures
        Vector<Double> weatherDates = new Vector<>();
        for (WeatherDate date : station) {
            weatherDates.add(date.getMaxTemperature());
        }

        Double[] arr = {-3.9, 3.6, 0.8, -5.0, -2.3, -2.0, -4.0, -7.9, 2.1, 2.5, 5.5, 5.4, -1.7, -4.8, 0.8, 4.9, 12.5, 11.7, 10.3, 31.8};
        ArrayList<Double> arrayList = new ArrayList<>(Arrays.asList(arr));
        Vector<Double> weatherDatesComparison = new Vector<>();
        weatherDatesComparison.addAll(arrayList);

        for (int i = 0; i < 16; i++) {
            assertEquals("Maximum temperatures don't match.", weatherDates.get(i), weatherDatesComparison.get(i));
        }

        // Verify solar radiation
        Vector<Double> radiationDates = new Vector<>();
        for (WeatherDate date : station) {
            radiationDates.add(date.getSolarRadiation());
        }

        Double[] arr2 = {2.3, 2.0, 4.3, 1.7, 8.1, 3.0, 1.4, 9.5, 8.6, 7.3, 9.0, 8.6, 5.7, 7.2, 9.8, 5.5, 6.1, 3.4, 6.0, 9.1};
        ArrayList<Double> arrayList2 = new ArrayList<>(Arrays.asList(arr2));
        Vector<Double> radiationDatesComparison = new Vector<>();
        radiationDatesComparison.addAll(arrayList2);

        for (int i = 0; i < 16; i++) {
            assertEquals("Solar radiations don't match.", radiationDates.get(i), radiationDatesComparison.get(i));
        }

        // Verify minimum temperatures
        Vector<Double> tempDates = new Vector<>();
        for (WeatherDate date : station) {
            tempDates.add(date.getMinTemperature());
        }

        Double[] arr3 = {-9.9, -9.9, -6.9, -6.6, -11.5, -7.6, -5.3, -19.9, -15.9, -11.9, -18.3, -21.3, -14.5, -14.9, -20.1, -15.0, -0.6, -7.5, -9.0, -1.7};
        ArrayList<Double> arrayList3 = new ArrayList<>(Arrays.asList(arr3));
        Vector<Double> tempDatesComparison = new Vector<>();
        tempDatesComparison.addAll(arrayList3);

        for (int i = 0; i < 16; i++) {
            assertEquals("Minimum temperatures don't match.", tempDatesComparison.get(i), tempDates.get(i));
        }

        // Verify precipitation
        Vector<Double> rainDates = new Vector<>();
        for (WeatherDate date : station) {
            rainDates.add(date.getPrecipitaiton());
        }

        Double[] arr4 = {0.0, 0.0, 0.0, 0.0, 0.0, 18.1, 1.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5.9, 0.0, 0.0, 0.0};
        ArrayList<Double> arrayList4 = new ArrayList<>(Arrays.asList(arr4));
        Vector<Double> rainDatesComparison = new Vector<>();
        rainDatesComparison.addAll(arrayList4);

        for (int i = 0; i < 16; i++) {
            assertEquals("Precipitation values don't match.", rainDates.get(i), rainDatesComparison.get(i));
        }

        // Verify dates
        Vector<GregorianCalendar> dates = new Vector<>();
        for (WeatherDate date : station) {
            dates.add(date.getDate());
        }

        String[] arr5 = {"20171", "20172", "20173", "20174", "20175", "20176", "20177", "20178", "20179", "201710",
                "201711", "201712", "201713", "201714", "201715", "201716", "201717", "201718", "201719", "201720"};

        for (int i = 0; i < 19; i++) {
            int year = dates.get(i).get(GregorianCalendar.YEAR);
            int day = dates.get(i).get(GregorianCalendar.DAY_OF_MONTH);
            String completeDate = "" + year + day;
            assertEquals("Dates don't match", completeDate, arr5[i]);
        }

    }


    @Test
    public void testFormatting() {

        WeatherFile station = getWeatherFile(WTH_TEST_FILE_NAME);

        // TODO complete

    }


    @Test
    public void testWriteFile() {

        WeatherStation station = new WeatherStation();
        station.setElevation(5);
        station.setInstitution("UFGA");
        station.setLatitude(28.620);
        station.setLongitude(-83.560);

        try {
            station.addDate(2017, 1, 1, 2.0, 5.0, 1.0, 2.0);
        } catch (IllegalWeatherDataException e) {
            fail("Incorrect weather date");
        }

        String station2 = Formatting.formatWTHFile(station);
        try {
            FileUtils.writeStringToFile(new File(WTH_TEST_TEMP_FILE_NAME), station2, forName("UTF-8"));
        } catch (IOException e) {
            fail("Unable to write to file");
        }

        WeatherFile file = getWeatherFile(WTH_TEST_TEMP_FILE_NAME);
        WeatherStation station3 = file.getStation();

        String inst = station3.getInstitution();
        assertEquals("Institutions don't match", "UFGA", inst);

        Double lat = station3.getLatitude();
        assertEquals("Latitudes don't match", 28.620, lat);

        Double lon = station3.getLongitude();
        assertEquals("Longitudes don't match", -83.560, lon);

        Integer elev = station3.getElevation();
        assertEquals("Elevations don't match", Integer.toString(5), Integer.toString(elev));

        for (WeatherDate date : station3) {
            assertEquals("Minimum temperatures don't match", 1.0, date.getMinTemperature());
            assertEquals("Maximum temperatures don't match", 5.0, date.getMaxTemperature());
            assertEquals("Solar radiaitions don't match", 2.0, date.getSolarRadiation());
            assertEquals("Precipitation values don't match", 2.0, date.getPrecipitaiton());
            assertEquals("Years don't match",2017,date.getDate().get(GregorianCalendar.YEAR));
            assertEquals("Months don't match",0,date.getDate().get(GregorianCalendar.MONTH));
            assertEquals("Days don't match",1,date.getDate().get(GregorianCalendar.DAY_OF_MONTH));
        }
    }

}

