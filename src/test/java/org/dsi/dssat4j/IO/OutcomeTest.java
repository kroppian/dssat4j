package org.dsi.dssat4j.IO;

import org.dsi.dssat4j.exceptions.FileParseException;
import org.junit.Test;
import org.dsi.dssat4j.IO.Outcome;

import java.io.File;
import java.io.IOException;
import static org.junit.Assert.*;

import static junit.framework.TestCase.fail;


public class OutcomeTest {

    double delta = 0.001;

    File OUTPUT_PATH_471 = new File("src/test/resources/DSSAT471/Maize/");
    File OUTPUT_PATH_470 = new File("src/test/resources/DSSAT470/Maize/");
    File OUTPUT_PATH_461 = new File("src/test/resources/DSSAT461/Maize/");

    private Outcome outcome471;
    private Outcome outcome470;
    private Outcome outcome461;

    private Outcome readOutcomeFile(File outputPath){
        Outcome outcome = null;
        try{
            outcome = new Outcome(outputPath);
        }catch(IOException e){
            System.err.println(e.getMessage());
            fail("IOException unexpected");
        }catch(FileParseException e){
            System.err.println(e.getMessage());
            fail("File Parse Exception unexpected");
        }
        return outcome;
    }

    public OutcomeTest(){
        outcome461 = readOutcomeFile(OUTPUT_PATH_461);
        outcome470 = readOutcomeFile(OUTPUT_PATH_470);
        outcome471 = readOutcomeFile(OUTPUT_PATH_471);
    }

    /*
     * Test methods
     */
    @Test
    public void testReadFile471() {
        readOutcomeFile(OUTPUT_PATH_471);
    }
    @Test
    public void testReadFile470() {
        readOutcomeFile(OUTPUT_PATH_470);
    }

    @Test
    public void testReadFile461() { readOutcomeFile(OUTPUT_PATH_461); }

    @Test
    public void testNitrogenLeaching471() {
        assertEquals(29.45,outcome471.getNitroLeaching(),delta);
    }
    @Test
    public void testNitrogenLeaching470() {
        assertEquals(40.58,outcome470.getNitroLeaching(),delta);
    }

    @Test
    public void testHarvestedYield471(){ assertEquals(112020,outcome471.getHarvestedYield(),delta); }
    @Test
    public void testHarvestedYield470(){ assertEquals(112021,outcome470.getHarvestedYield(),delta); }
    @Test
    public void testHarvestedYield461(){ assertEquals(2020, outcome461.getHarvestedYield(), delta); }


    @Test
    public void testByProductYield471(){ assertEquals(654462,outcome471.getByProductYield(), delta); }
    @Test
    public void testByProductYield470(){ assertEquals(224466,outcome470.getByProductYield(), delta); }
    @Test
    public void testByProductYield461(){assertEquals(4466,outcome461.getByProductYield(), delta);}


    @Test
    public void testNitrogenApplied471(){ assertEquals(29636,outcome471.getNitrogenApplied(),delta); }
    @Test
    public void testNitrogenApplied470(){ assertEquals(53116,outcome470.getNitrogenApplied(),delta); }
    @Test
    public void testNitrogenApplied461(){ assertEquals(116,outcome461.getNitrogenApplied(),delta); }

    @Test
    public void testNumberOfNitrogenApplications471(){
        assertEquals(8948,outcome471.getNumberOfNitrogenApplications(),delta);
    }
    @Test
    public void testNumberOfNitrogenApplications470(){
        assertEquals(12123,outcome470.getNumberOfNitrogenApplications(),delta);
    }
    @Test
    public void testNumberOfNitrogenApplications461(){
        assertEquals(3,outcome461.getNumberOfNitrogenApplications(),delta);
    }

    @Test
    public void testIrrigationApplied471(){
        assertEquals(39413,outcome471.getIrrigationApplied(),delta);
    }
    @Test
    public void testIrrigationApplied470(){
        assertEquals(12413,outcome470.getIrrigationApplied(),delta);
    }
    @Test
    public void testIrrigationApplied461(){
        assertEquals(13,outcome461.getIrrigationApplied(),delta);
    }

    @Test
    public void testNumberOfIrrigationApplications471(){
        assertEquals(30498,outcome471.getNumberOfIrrigationApplictions(), delta);
    }
    @Test
    public void testNumberOfIrrigationApplications470(){
        assertEquals(21231,outcome470.getNumberOfIrrigationApplictions(), delta);
    }
    @Test
    public void testNumberOfIrrigationApplications461(){
        assertEquals(1,outcome461.getNumberOfIrrigationApplictions(), delta);
    }

    @Test
    public void testWeightOfPlantingMartial471(){
        assertEquals(23992,outcome471.getWeightOfPlantingMaterial(), delta);
    }
    @Test
    public void testWeightOfPlantingMartial470(){
        assertEquals(65344,outcome470.getWeightOfPlantingMaterial(), delta);
    }
    @Test
    public void testWeightOfPlantingMartial461(){
        assertEquals(8192,outcome461.getWeightOfPlantingMaterial(), delta);
    }



    @Test
    public void testResidueApplied471(){
        assertEquals(8192,outcome471.getResidueApplied(), delta);
    }
    @Test
    public void testResidueApplied470(){
        assertEquals(47341,outcome470.getResidueApplied(), delta);
    }
    @Test
    public void testResidueApplied461(){
        assertEquals( 8919,outcome461.getResidueApplied(), delta);
    }

    @Test
    public void testPhosphorusApplied471(){
        assertEquals(92109,outcome471.getPhosphorusApplied(), delta);
    }
    @Test
    public void testPhosphorusApplied470(){
        assertEquals(94342,outcome470.getPhosphorusApplied(), delta);
    }
    @Test
    public void testPhosphorusApplied461(){
        assertEquals(9012,outcome461.getPhosphorusApplied(), delta);
    }

    @Test
    public void testNumberOfPhosphorusApplications471(){
        assertEquals(21954,outcome471.getNumberOfPhosphorusApplications(), delta);
    }
    @Test
    public void testNumberOfPhosphorusApplications470(){
        assertEquals(85302,outcome470.getNumberOfPhosphorusApplications(), delta);
    }
    @Test
    public void testNumberOfPhosphorusApplications461(){
        assertEquals(9102,outcome461.getNumberOfPhosphorusApplications(), delta);
    }


}



