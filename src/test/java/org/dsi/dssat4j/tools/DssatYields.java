package org.dsi.dssat4j.tools;

import org.dsi.dssat4j.IO.Experiment;
import org.dsi.dssat4j.IO.Parsers;
import org.dsi.dssat4j.beans.FertilizerApps;
import org.dsi.dssat4j.beans.IrrigationApps;
import org.dsi.dssat4j.beans.ResourceApp;
import org.dsi.dssat4j.beans.Treatment;
import org.dsi.dssat4j.exceptions.FailedRunException;
import org.dsi.dssat4j.exceptions.FileParseException;
import org.dsi.dssat4j.exceptions.IllegalApplicationException;

import java.io.File;
import java.io.IOException;

import org.dsi.dssat4j.IO.Outcome;
import org.dsi.dssat4j.exceptions.SessionException;

import static junit.framework.TestCase.fail;

public class DssatYields {

    private static File EXP = new File("src/test/resrouces/DSSAT471/Maize/UFGA8201.MZX");

    private static ResourceApp.Method METHOD = ResourceApp.Method.IR004;

    // [ [YEAR,MONTH,DAY,AMOUNT], [YEAR,MONTH,DAY,AMOUNT], [YEAR,MONTH,DAY,AMOUNT] ... ]
    public static int ObtainYield(DssatConfig config, int[][] IrrigationAmount, int[][] FertilizerAmount) {



        Experiment exp = null;

        try {
            exp = Parsers.parseExpFile(EXP);
        } catch (IOException e) {
            System.err.println("Unable to find exp file.");
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (FileParseException e) {
            System.err.println("Unable to parse exp file.");
            System.err.println(e.getMessage());
            System.exit(1);
        }

        exp.clearTreatments();

        IrrigationApps irrigation = new IrrigationApps();
        FertilizerApps fertilizer = new FertilizerApps();

        try {
            for(int i = 0; i < IrrigationAmount.length;++i){
                irrigation.addIrrApp(IrrigationAmount[i][0],IrrigationAmount[i][1], IrrigationAmount[i][2], METHOD, IrrigationAmount[i][3]);
            }
            for(int i = 0; i < FertilizerAmount.length;++i){
                fertilizer.addNitroApp(IrrigationAmount[i][0],IrrigationAmount[i][1], IrrigationAmount[i][2], METHOD, IrrigationAmount[i][3], 7);
            }

        } catch (IllegalApplicationException e) {
            System.err.println("Illegal app exception");
            System.err.println(e.getMessage());
            System.exit(1);
        }

        exp.clearTreatments();
        exp.clearFertilizerLevels();
        exp.clearIrrigationLevels();
        Treatment treatment = new Treatment();
        treatment.setTreatmentNumber(1);
        treatment.setIrrigationLevel(1);
        treatment.setFertilizerLevel(1);
        exp.setTreatment(treatment);
        exp.setIrrigationLevel(1, irrigation);
        exp.setFertilizerLevel(1, fertilizer);

        DssatSessionFactory seshFact = new DssatSessionFactory();
        DssatSession sesh  = null;
        try{
            sesh = seshFact.createSession(config);
        }catch(SessionException e){
            fail("Error setting up the DSSAT session: \n" + e.getMessage());
        }

        DssatRunner runner = null;
        try{
            runner = sesh.getDssatRunner();
        }catch(SessionException e){
            fail("Run failed while getting runner:\n" + e.getMessage());
        }

        Outcome outcome = null;

        try {
            outcome = runner.run(exp,1);
        } catch (FailedRunException i) {
            System.err.println("Run failed");
            System.err.println(i.getMessage());
            System.exit(1);
        }

        int yield = outcome.getYield();
        return yield;
    }
}
