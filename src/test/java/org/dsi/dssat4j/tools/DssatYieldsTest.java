package org.dsi.dssat4j.tools;

import org.dsi.dssat4j.beans.FertilizerApps;
import org.dsi.dssat4j.beans.IrrigationApps;
import org.dsi.dssat4j.tools.DssatConfig;
import org.dsi.dssat4j.tools.DssatYields;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class DssatYieldsTest {

    // TODO get this working on Linux elegantly. For now, commenting out tests to allow the project to build

    private static File RUN_DIR = new File("src/test/resources/testRunDirectory");
    private static File EXE = new File(System.getenv("DSSAT_EXE"));
    private static File PROFILE = new File(System.getenv("DSSAT_PROF"));


    //@Test
    public void TestTreatmentOne() {

        DssatConfig config = null;
        try{
            config =  new DssatConfig(RUN_DIR, EXE, PROFILE);
        }catch(IOException e){
            fail("Bad given config file:\n" + e.getMessage());
        }

        DssatYields data = new DssatYields();

        // [[year,month,day,amount],[year,month,day,amount]...]
        int[][] irr = {{1982,3,4,1},{1982,3,5,2},{1982,3,6,1},{1982,3,7,3}};
        int[][] fer = {{1982,4,7,1},{1982,4,12,1},{1982,3,15,2},{1982,3,30,2}};

        int yield = data.ObtainYield(config,irr,fer);
        assertEquals(yield, 781);
    }

    //@Test
    public void TestTreatmentTwo() {


        DssatConfig config = null;
        try{
            config =  new DssatConfig(RUN_DIR, EXE, PROFILE);
        }catch(IOException e){
            fail("Bad given config file:\n" + e.getMessage());
        }

        DssatYields data = new DssatYields();

        int[][] irr = {{1992,3,25,1},{1992,4,17,1},{1992,5,10,1}};
        int[][] fer = {{1992,3,25,2},{1992,4,17,2},{1992,5,10,2}};

        int yield = data.ObtainYield(config,irr,fer);
        assertEquals(yield, 755);
    }

    //@Test
    public void TestTreatmentThree() {

        DssatConfig config = null;
        try{
            config =  new DssatConfig(RUN_DIR, EXE, PROFILE);
        }catch(IOException e){
            fail("Bad given config file:\n" + e.getMessage());
        }

        DssatYields data = new DssatYields();

        int[][] irr = {{1982,3,4,2}};
        int[][] fer = {{1982,4,7,1}};

        int yield = data.ObtainYield(config,irr,fer);
        assertEquals(yield, 756);
    }

    //@Test
    public void TestTreatmentFour() {
        DssatConfig config = null;
        try{
            config =  new DssatConfig(RUN_DIR, EXE, PROFILE);
        }catch(IOException e){
            fail("Bad given config file:\n" + e.getMessage());
        }

        DssatYields data = new DssatYields();

        int[][] irr = {{1982,3,4,1},{1982,4,17,1},{1982,5,10,1},{1982,6,11,1},{1982,6,22,2}};
        int[][] fer = {{1982,3,25,1},{1982,4,17,2},{1982,5,10,2},{1982,6,11,1},{1982,6,22,2}};

        int yield = data.ObtainYield(config,irr,fer);
        assertEquals(yield, 817);
    }

}
