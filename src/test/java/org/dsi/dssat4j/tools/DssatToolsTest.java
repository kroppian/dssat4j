package org.dsi.dssat4j.tools;

import static org.junit.Assert.*;

import org.dsi.dssat4j.beans.ResourceApp;
import org.junit.Test;

import org.dsi.dssat4j.exceptions.IllegalApplicationException;
import org.dsi.dssat4j.beans.IrrigationApps;

/**
 * Created by kroppian on 6/19/2017.
 */
public class DssatToolsTest {

    @Test
    public void testRunObjective() throws IllegalApplicationException {
        // Kind of a dumb test. More of a place holder
        IrrigationApps resourceInput = new IrrigationApps();
        resourceInput.addIrrApp(2016, 11, 4, ResourceApp.Method.IR004, 50);
        resourceInput.addIrrApp(2009, 10, 2, ResourceApp.Method.IR004, 34);
        //DssatTools.runObjective(resourceInput);
        assertEquals(true, true);
    }

}
