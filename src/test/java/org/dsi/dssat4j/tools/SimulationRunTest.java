package org.dsi.dssat4j.tools;

import org.apache.commons.io.FileUtils;
import org.dsi.dssat4j.IO.Experiment;
import org.dsi.dssat4j.IO.Outcome;
import org.dsi.dssat4j.IO.Parsers;
import org.dsi.dssat4j.exceptions.FailedRunException;
import org.dsi.dssat4j.exceptions.FileParseException;
import org.dsi.dssat4j.exceptions.SessionException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.fail;

public class SimulationRunTest {

    private final String S = File.separator;

    private final File EXP_FILE = new File("src" + S + "test" + S + "resources" + S + "DSSAT471" + S + "Maize" + S + "UFGA8202.MZX");
    private final File PROFILE = new File(System.getenv("DSSAT_PROF"));
    //private final File PROFILE_LIN = new File("src/test/resources/DSSAT471/DSSATPRO.L47");
    //private final File RUN_DIR = new File("src/test/resources/testRunDirectory");
    private final File RUN_DIR = new File(S + "tmp" + S + "dssat4j");
    private final File EXE_FILE = new File(System.getenv("DSSAT_EXE")) ;
    private DssatSession sesh;

    public SimulationRunTest() {
        RUN_DIR.mkdir();

        File profile = PROFILE;


        DssatConfig config = null;
        try{
            config =  new DssatConfig(RUN_DIR, EXE_FILE, profile);
        }catch(IOException e){
            fail("Bad given config file:\n" + e.getMessage());
        }

        DssatSessionFactory seshFact = new DssatSessionFactory();

        try{
            this.sesh = seshFact.createSession(config);
        }catch(SessionException e){
            fail("Error creating DSSAT session\n" + e.getMessage());
        }
        try{
            FileUtils.forceDeleteOnExit(RUN_DIR);
        }catch(IOException e){
            fail("Error trying to set force-delete-on-exit for our run dir:\n" + e.getMessage());
        }
    }

    /*
     * Test methods
     */
    @Test
    public void testReadFile() {

        Experiment exp = null;

        try{
            exp = Parsers.parseExpFile(EXP_FILE);
        }catch(IOException e){
            fail("Unable to read the experiment file\n" + e.getMessage());
        }catch(FileParseException e){
            fail("Unable to parse the experiment file\n" + e.getMessage());
        }

        Outcome out = null;
        try {
            DssatRunner runner = this.sesh.getDssatRunner();
            out = runner.run(exp, 1);
        }catch(FailedRunException e){
            fail("Run failed\n" + e.getMessage());
        }catch(SessionException e){
            fail("Session creation failed\n" + e.getMessage());
        }

    }


    @Test
    public void testWrongTreatment() {

        Experiment exp = null;

        try{
            exp = Parsers.parseExpFile(EXP_FILE);
        }catch(IOException e){
            fail("Unable to read the experiment file\n" + e.getMessage());
        }catch(FileParseException e){
            fail("Unable to parse the experiment file\n" + e.getMessage());
        }


        Outcome out = null;
        try{
            out = sesh.getDssatRunner().run(exp, 0);
        }catch(FailedRunException e){
            try{
                out = sesh.getDssatRunner().run(exp, 7);
            }catch(FailedRunException ex){
                return;
            }catch(SessionException ex){
                fail("No expecting session exception: \n" + e.getMessage());
            }
        }catch(SessionException e){
            fail("Not expecting a session exception:\n" + e.getMessage() );
        }
        fail("Should have failed running against a non-existant treatment");

    }

    @Test
    public void testRightTreatments() {

        Experiment exp = null;

        try{
            exp = Parsers.parseExpFile(EXP_FILE);
        }catch(IOException e){
            fail("Unable to read the experiment file\n" + e.getMessage());
        }catch(FileParseException e){
            fail("Unable to parse the experiment file\n" + e.getMessage());
        }

        DssatRunner runner = null;
        try{
            runner = sesh.getDssatRunner();
        }catch(SessionException e){
            fail("Not expecting a session exception");
        }

        Outcome out = null;
        try{
            for(int i = 1; i < 7; i++){
                out = runner.run(exp,i);
                out.getYield();
            }
            runner.retireRunner();
        }catch(FailedRunException e){
            fail("Unable to parse the experiment file\n" + e.getMessage());
        }



    }



}



